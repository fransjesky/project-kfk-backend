[![MEAN.JS Logo](http://meanjs.org/img/logo-small.png)](http://meanjs.org/)

[![Gitter](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/meanjs/mean?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)
[![Build Status](https://travis-ci.org/meanjs/mean.svg?branch=master)](https://travis-ci.org/meanjs/mean)
[![Dependencies Status](https://david-dm.org/meanjs/mean.svg)](https://david-dm.org/meanjs/mean)
[![Coverage Status](https://coveralls.io/repos/meanjs/mean/badge.svg?branch=master&service=github)](https://coveralls.io/github/meanjs/mean?branch=master)
[![Known Vulnerabilities](https://snyk.io/test/github/meanjs/mean/badge.svg)](https://snyk.io/test/github/meanjs/mean)

MEAN.JS is a full-stack JavaScript open-source solution, which provides a solid starting point for [MongoDB](http://www.mongodb.org/), [Node.js](http://www.nodejs.org/), [Express](http://expressjs.com/), and [AngularJS](http://angularjs.org/) based applications. The idea is to solve the common issues with connecting those frameworks, build a robust framework to support daily development needs, and help developers use better practices while working with popular JavaScript components.

## Before You Begin
Before you begin we recommend you read about the basic building blocks that assemble 
* Express - The best way to understand express is through its [Official Website](http://expressjs.com/), which has a [Getting Started](http://expressjs.com/starter/installing.html) guide, as well as an [ExpressJS](http://expressjs.com/en/guide/routing.html) guide for general express topics. You can also go through this [StackOverflow Thread](http://stackoverflow.com/questions/8144214/learning-express-for-node-js) for more resources.
* Node.js - Start by going through [Node.js Official Website](http://nodejs.org/) and this [StackOverflow Thread](http://stackoverflow.com/questions/2353818/how-do-i-get-started-with-node-js), which should get you going with the Node.js platform in no time.

## Quick Install
Once you've downloaded the boilerplate and installed all the prerequisites, you're just a few steps away from starting to develop your MEAN application.

The boilerplate comes pre-bundled with a `package.json` and `bower.json` files that contain the list of modules you need to start your application.

To install the dependencies, run this in the application folder from the command-line:

```bash
$ npm install
```

This command does a few things:
* First it will install the dependencies needed for the application to run.
* If you're running in a development environment, it will then also install development dependencies needed for testing and running your application.
* When the npm packages install process is over, npm will initiate a bower install command to install all the front-end modules needed for the application
* To update these packages later on, just run `npm update`

## Running Your Application

Run your application using yarn:

```bash
$ yarn start
```

Your application should run on port 3001 with the *development* environment configuration, so in your browser just go to [http://localhost:3001](http://localhost:3001)

That's it! Your application should be running. To proceed with your development, check the other sections in this documentation.
If you encounter any problems, try the Troubleshooting section.

Explore `config/env/development.js` for development environment configuration options.

### Running in Production mode
To run your application with *production* environment configuration:

```bash
$ npm run start:prod
```

Explore `config/env/production.js` for production environment configuration options.

### Running with User Seed
To have default account(s) seeded at runtime:

In Development:
```bash
MONGO_SEED=true npm start
```
It will try to seed the users 'user' and 'admin'. If one of the user already exists, it will display an error message on the console. Just grab the passwords from the console.

In Production:
```bash
MONGO_SEED=true npm start:prod
```
This will seed the admin user one time if the user does not already exist. You have to copy the password from the console and save it.

### Running with TLS (SSL)
Application will start by default with secure configuration (SSL mode) turned on and listen on port 8443.
To run your application in a secure manner you'll need to use OpenSSL and generate a set of self-signed certificates. Unix-based users can use the following command:

```bash
$ npm run generate-ssl-certs
```

Windows users can follow instructions found [here](http://www.websense.com/support/article/kbarticle/How-to-use-OpenSSL-and-Microsoft-Certification-Authority).
After you've generated the key and certificate, place them in the *config/sslcerts* folder.

Finally, execute prod task `npm run start:prod`
* enable/disable SSL mode in production environment change the `secure` option in `config/env/production.js`


## Testing Your Application
You can run the full test suite included with MEAN.JS with the test task:

```bash
$ npm test
```
This will run both the server-side tests (located in the `app/tests/` directory) and the client-side tests (located in the `public/modules/*/tests/`).

To execute only the server tests, run the test:server task:

```bash
$ npm run test:server
```

To execute only the server tests and run again only changed tests, run the test:server:watch task:

```bash
$ npm run test:server:watch
```

And to run only the client tests, run the test:client task:

```bash
$ npm run test:client
```

## Running your application with Gulp

The MEAN.JS project integrates Gulp as build tools and task automation.

We have wrapped Gulp tasks with npm scripts so that regardless of the build tool running the project is transparent to you.

To use Gulp directly, you need to first install it globally:

```bash
$ npm install gulp -g
```

Then start the development environment with:

```bash
$ gulp
```

To run your application with *production* environment configuration, execute gulp as follows:

```bash
$ gulp prod
```

It is also possible to run any Gulp tasks using npm's run command and therefore use locally installed version of gulp, for example: `npm run gulp eslint`

## Development and deployment With Docker

* Install [Docker](https://docs.docker.com/installation/#installation)
* Install [Compose](https://docs.docker.com/compose/install/)

* Local development and testing with compose:
```bash
$ docker-compose up
```

* Local development and testing with just Docker:
```bash
$ docker build -t mean .
$ docker run -p 27017:27017 -d --name db mongo
$ docker run -p 3000:3000 --link db:db_1 mean
$
```

* To enable live reload, forward port 35729 and mount /app and /public as volumes:
```bash
$ docker run -p 3000:3000 -p 35729:35729 -v /Users/mdl/workspace/mean-stack/mean/public:/home/mean/public -v /Users/mdl/workspace/mean-stack/mean/app:/home/mean/app --link db:db_1 mean
```

### Production deploy with Docker

* Production deployment with compose:
```bash
$ docker-compose -f docker-compose-production.yml up -d
```

* Production deployment with just Docker:
```bash
$ docker build -t mean -f Dockerfile-production .
$ docker run -p 27017:27017 -d --name db mongo
$ docker run -p 3000:3000 --link db:db_1 mean
```

## Deploying to PAAS

###  Deploying MEANJS To Heroku

By clicking the button below you can signup for Heroku and deploy a working copy of MEANJS to the cloud without having to do the steps above.

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy)

### Amazon S3 configuration

To save the profile images to S3, simply set those environment variables:
UPLOADS_STORAGE: s3
S3_BUCKET: the name of the bucket where the images will be saved
S3_ACCESS_KEY_ID: Your S3 access key
S3_SECRET_ACCESS_KEY: Your S3 access key password

## Getting Started With MEAN.JS
You have your application running, but there is a lot of stuff to understand. We recommend you go over the [Official Documentation](http://meanjs.org/docs.html).
In the docs we'll try to explain both general concepts of MEAN components and give you some guidelines to help you improve your development process. We tried covering as many aspects as possible, and will keep it updated by your request. You can also help us develop and improve the documentation by checking out the *gh-pages* branch of this repository.

## Community
* Use the [Official Website](http://meanjs.org) to learn about changes and the roadmap.
* Join #meanjs on freenode.
* Discuss it in the new [Google Group](https://groups.google.com/d/forum/meanjs)
* Ping us on [Twitter](http://twitter.com/meanjsorg) and [Facebook](http://facebook.com/meanjs)

## Contributing
We welcome pull requests from the community! Just be sure to read the [contributing](https://github.com/meanjs/mean/blob/master/CONTRIBUTING.md) doc to get started.

## Credits
Inspired by the great work of [Madhusudhan Srinivasa](https://github.com/madhums/)
The MEAN name was coined by [Valeri Karpov](http://blog.mongodb.org/post/49262866911/the-mean-stack-mongodb-expressjs-angularjs-and).


## Get started
Execute query script below into new database to create existing data.

```
INSERT INTO public."Users" ("firstName","lastName","displayName",email,"password",salt,"profileImageURL",category,provider,"providerData","additionalProvidersData","resetPasswordToken","resetPasswordExpires","registerToken","registerTokenExpires",status,"createdAt","updatedAt") VALUES 
('Admin','Xcidic','Admin Xcidic','admin@localhost.com','cmIxRde3ntCVHnAhrlCtwb1k1JdX635Hg1C7MlFvviCgriDqwmDE12HZIkTntu1FdyCPnX02ZwyqIvaYq18e4A==','MtlT2L9Ri7ICQjFnATaRqg==','https://storage.googleapis.com/blog.xcidic.com/vanilla-default/default.png','admin','local',NULL,NULL,NULL,NULL,'4ab49f658764a277e7138423e895c02543259b57','2019-04-24 14:47:49.838','approved','2019-04-23 14:47:49.839','2019-04-23 14:47:49.839')
;

INSERT INTO public."Roles" ("name","createdAt","updatedAt","createdBy") VALUES 
('admin','2019-04-23 14:47:22.113','2019-04-23 14:47:22.113',NULL)
('customer','2019-04-23 14:47:22.113','2019-04-23 14:47:22.113',NULL)
;

INSERT INTO public."UserRoles" ("createdAt","updatedAt","roleId","userId") VALUES 
('2019-04-23 14:47:50.193','2019-04-23 14:47:50.193',1,1)
;

INSERT INTO public."MasterMenus" ("_id",title,icon,"iconGrid",state,"isFavorite","countClick","type",abstract,"position","createdAt","updatedAt","parentId","createdBy") VALUES 
(1,'Admin','person','','',false,0,'dropdown',true,2,'2018-08-21 17:38:38.498','2018-08-21 17:38:38.498',NULL,NULL)
,(2,'My Profile','account_box','https://firebasestorage.googleapis.com/v0/b/vanilla-react.appspot.com/o/userRoles.svg?alt=media&token=9cb2fa0f-cf99-402c-91a5-92d985acb6de','/profiles',false,0,'',false,1,'2018-08-21 17:38:38.498','2018-08-21 17:38:38.498',NULL,NULL)
,(3,'Users','group','https://firebasestorage.googleapis.com/v0/b/vanilla-react.appspot.com/o/aclRoles.svg?alt=media&token=6d199e31-8e9c-4add-a611-761777f10b83','/users',false,0,'',false,1,'2018-08-21 17:38:38.498','2018-08-21 17:38:38.498',1,NULL)
,(4,'User Roles','portrait','https://firebasestorage.googleapis.com/v0/b/vanilla-react.appspot.com/o/userRoles.svg?alt=media&token=9cb2fa0f-cf99-402c-91a5-92d985acb6de','/user-roles',false,0,'',false,2,'2018-08-21 17:38:38.498','2018-08-21 17:38:38.498',1,NULL)
,(5,'ACL Roles','dns','https://firebasestorage.googleapis.com/v0/b/vanilla-react.appspot.com/o/aclRoles.svg?alt=media&token=6d199e31-8e9c-4add-a611-761777f10b83','/acl-roles',false,0,'',false,3,'2018-08-21 17:38:38.498','2018-08-21 17:38:38.498',1,NULL)
,(6,'Master Menu','list_alt','https://firebasestorage.googleapis.com/v0/b/vanilla-react.appspot.com/o/masterMenus.svg?alt=media&token=a6793a37-a516-4b70-a546-df2b2e49880b','/master-menus',false,0,'',false,4,'2018-08-21 17:38:38.498','2018-08-21 17:38:38.498',1,NULL)
;

INSERT INTO public."RoleMenus" ("_id","createdAt","updatedAt","menuId","roleId") VALUES 
(1,'2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',1,1)
,(2,'2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',2,1)
,(3,'2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',3,1)
,(4,'2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',4,1)
,(5,'2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',5,1)
,(6,'2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',6,1)
;

INSERT INTO public."Acls" ("module","action",resource,"permission","createdAt","updatedAt","userId") VALUES 
('users','me','/api/users/me','get','2018-08-21 17:38:38.493','2018-08-21 17:38:38.493',NULL)
,('users','update','/api/users/me','put','2018-08-21 17:38:38.493','2018-08-21 17:38:38.493',NULL)
,('users','list','/api/users','get','2018-08-21 17:38:38.493','2018-08-21 17:38:38.493',NULL)
,('users','create','/api/users','post','2018-08-21 17:38:38.493','2018-08-21 17:38:38.493',NULL)
,('users','bulkApprove','/api/users/bulk-approve','post','2018-08-21 17:38:38.493','2018-08-21 17:38:38.493',NULL)
,('users','bulkDelete','/api/users/bulk-delete','post','2018-08-21 17:38:38.493','2018-08-21 17:38:38.493',NULL)
,('users','read','/api/users/:userId','get','2018-08-21 17:38:38.493','2018-08-21 17:38:38.493',NULL)
,('users','update','/api/users/:userId','put','2018-08-21 17:38:38.493','2018-08-21 17:38:38.493',NULL)
,('users','delete','/api/users/:userId','delete','2018-08-21 17:38:38.493','2018-08-21 17:38:38.493',NULL)
,('users','approve','/api/users/:userId/approve','post','2018-08-21 17:38:38.493','2018-08-21 17:38:38.493',NULL)
,('user-roles','create','/api/user-roles','post','2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',NULL)
,('user-roles','list','/api/user-roles','get','2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',NULL)
,('user-roles','update','/api/user-roles/:userRoleId','put','2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',NULL)
,('user-roles','read','/api/user-roles/:userRoleId','get','2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',NULL)
,('user-roles','delete','/api/user-roles/:userRoleId','delete','2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',NULL)
,('user-roles','bulkDelete','/api/user-roles/bulk-delete','post','2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',NULL)
,('acls','create','/api/acl-roles','post','2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',NULL)
,('acls','list','/api/acl-roles','get','2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',NULL)
,('acls','update','/api/acl-roles/:aclRoleId','put','2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',NULL)
,('acls','read','/api/acl-roles/:aclRoleId','get','2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',NULL)
,('acls','delete','/api/acl-roles/:aclRoleId','delete','2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',NULL)
,('acls','bulkDelete','/api/acl-roles/bulk-delete','post','2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',NULL)
,('master-menus','create','/api/master-menus','post','2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',NULL)
,('master-menus','list','/api/master-menus','get','2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',NULL)
,('master-menus','update','/api/master-menus/:masterMenuId','put','2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',NULL)
,('master-menus','read','/api/master-menus/:masterMenuId','get','2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',NULL)
,('master-menus','delete','/api/master-menus/:masterMenuId','delete','2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',NULL)
,('master-menus','bulkDelete','/api/master-menus/bulk-delete','post','2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',NULL)
,('master-menus','countClick','/api/master-menus/:masterMenuId/count','put','2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',NULL)
;

INSERT INTO public."AclRoles" ("createdAt","updatedAt","aclId","roleId") VALUES 
('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',1,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',2,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',3,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',4,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',5,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',6,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',7,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',8,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',9,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',10,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',11,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',12,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',13,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',14,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',15,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',16,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',17,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',18,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',19,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',20,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',21,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',22,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',23,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',24,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',25,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',26,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',27,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',28,1)
,('2019-04-23 14:47:22.000','2019-04-23 14:47:22.000',29,1)
;

```

## Best Practice
* [Performance](https://expressjs.com/en/advanced/best-practice-performance.html)
* [Security](https://github.com/lirantal/awesome-nodejs-security)

## [1.0.0] - 2020-10-15
### Added
- Fork project using MEAN JS version 0.6.0
- Remove unused dependencies

## License
[The MIT License](LICENSE.md)
