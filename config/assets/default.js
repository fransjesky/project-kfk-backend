'use strict';

/* eslint comma-dangle:[0, "only-multiline"] */

module.exports = {
  gulpConfig: ['gulpfile.js'],
  allJS: [
    'server.js',
    'config/**/*.js',
    'modules/*/**/*.js',
    'modules/*/*/**/*.js',
    'modules/*/*/*/**/*.js',
  ],
  models: [
    'modules/*/models/**/*.js',
    'modules/*/*/models/**/*.js',
    'modules/*/*/*/models/**/*.js',
  ],
  routes: [
    'modules/!(core)/routes/**/*.js',
    'modules/*/*/routes/**/*.js',
    'modules/*/*/*/routes/**/*.js',
    'modules/core/routes/**/*.js',
  ],
  sockets: [
    'modules/*/sockets/**/*.js',
    'modules/*/*/sockets/**/*.js',
    'modules/*/*/*/sockets/**/*.js',
  ],
  config: [
    'modules/*/config/*.js',
    'modules/*/*/config/*.js',
    'modules/*//**/config/*.js',
  ],
  policies: [
    'modules/*/policies/*.js',
    'modules/*/*/policies/*.js',
    'modules/*/*/*/policies/*.js',
  ],
  cronjobs: [
    'modules/*/cronjobs/*.js',
    'modules/*/*/cronjobs/*.js',
    'modules/*/*/*/cronjobs/*.js',
  ],
  views: [
    'modules/*/views/*.html',
    'modules/*/*/views/*.html',
    'modules/*/*/*/views/*.html',
  ],
};
