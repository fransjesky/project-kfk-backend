'use strict';
const aws = require('aws-sdk');
const path = require('path');

const awsUpload = {
  bucketName: process.env.AWS_BUCKET_NAME || 'kbri-lima.storage.com',
  acl: process.env.AWS_ACL || 'public-read',
  secretAccessKey:
    process.env.AWS_SECRET_ACCESS_KEY ||
    'DluFs+0I/mZuML4xEQCEQmoGj0SfVH+tLJGgBqDW',
  accessKeyId: process.env.AWS_ACCESS_ID || 'AKIAWBIHNQ2ONIS4GRF3',
  region: process.env.AWS_REGION || 'ap-southeast-1',
  environment: process.env.STORAGE_ENV || 'development',
};

aws.config.update({
  secretAccessKey: awsUpload.secretAccessKey, // Not working key, Your SECRET ACCESS KEY from AWS should go here, never share it!!!
  accessKeyId: awsUpload.accessKeyId, // Not working key, Your ACCESS KEY ID from AWS should go here, never share it!!!
  region: awsUpload.region, // region of your bucket
});

const s3 = new aws.S3();
awsUpload.s3 = s3;

module.exports = {
  app: {
    title: 'project-xcidic4.0-backend',
    description: 'Backend project with Sequelize, Express, and Node.js',
    keywords: 'sequelize, express, node.js, passport',
    googleAnalyticsTrackingID:
      process.env.GOOGLE_ANALYTICS_TRACKING_ID ||
      'GOOGLE_ANALYTICS_TRACKING_ID',
  },
  testing: process.env.TESTING === 'testing' ? '[TEST] ' : '',
  db: {
    promise: global.Promise,
  },
  templateEngine: 'swig',
  port: process.env.PORT || 3001,
  host: process.env.HOST || '0.0.0.0',
  // DOMAIN config should be set to the fully qualified application accessible
  // URL. For example: https://www.myapp.com (including port if required).
  domain: process.env.DOMAIN || 'http://18.231.186.80:3000', //'https://dev-dashboard.kbri-lima.com'
  // Session Cookie settings
  sessionCookie: {
    // session expiration is set by default to 24 hours
    maxAge: 24 * (60 * 60 * 1000),
    // httpOnly flag makes sure the cookie is only accessed
    // through the HTTP protocol and not JS/browser
    httpOnly: true,
    // secure cookie should be turned to true to provide additional
    // layer of security so that the cookie is set only when working
    // in HTTPS mode.
    secure: false,
  },
  // sessionSecret should be changed for security measures and concerns
  sessionSecret: process.env.SESSION_SECRET || 'MEAN',
  // sessionKey is the cookie session name
  sessionKey: 'sessionId',
  sessionCollection: 'sessions',
  // Lusca config
  csrf: {
    csrf: false,
    csp: false,
    xframe: 'SAMEORIGIN',
    p3p: 'ABCDEF',
    xssProtection: true,
  },
  jwt: {
    secret: process.env.JWT_SECRET || 'kbri-lima-dev',
    refreshTokenSecret:
      process.env.JWT_REFRESH_SECRET || 'kbri-lima-refresh-secret',
    refreshOptions: {
      expiresIn: process.env.JWT_REFRESH_EXPIRES_IN || 31557600000,
    },
    options: {
      expiresIn: process.env.JWT_EXPIRES_IN || '24h',
    },
  },
  sentry: {
    dsn:
      process.env.SENTRY_DSN ||
      'https://1a5c27d1ac914e60973997f3320e6621@o516873.ingest.sentry.io/5628630',
    environment: process.env.SENTRY_ENVIRONMENT || 'development',
  },
  oneSignal: {
    userAuthKey:
      process.env.OS_USER_AUTH ||
      '*********************************************MzJk',
    app: {
      appAuthKey:
        process.env.OS_APP_AUTH ||
        'OWQ4ZTAzYWYtN2U5Zi00MDQ1LTgwOWQtZGYzZTc5MTI4ODBh',
      appId: process.env.OS_APP_ID || 'b2bb7dbb-5fff-4abf-8760-d122ec471c7e',
    },
  },
  logo: 'modules/core/client/img/brand/logo.png',
  // favicon: 'modules/core/client/img/brand/favicon.ico',
  illegalUsernames: [
    'meanjs',
    'administrator',
    'password',
    'admin',
    'user',
    'unknown',
    'anonymous',
    'null',
    'undefined',
    'api',
  ],
  uploads: {
    // Storage can be 'local' or 's3'
    storage: process.env.UPLOADS_STORAGE || 'local',
    profile: {
      image: {
        dest: './modules/users/client/img/profile/uploads/',
        limits: {
          fileSize: 1 * 1024 * 1024, // Max file size in bytes (1 MB)
        },
      },
    },
    awsUpload,
  },
  importUpload: {
    dest: './uploads/', // Compliance upload destination path
    limits: {
      fileSize: 10 * 1024 * 1024, // Max file size in bytes (10 MB)
    },
  },
  shared: {
    owasp: {
      allowPassphrases: true,
      maxLength: 128,
      minLength: 10,
      minPhraseLength: 20,
      minOptionalTestsToPass: 4,
    },
  },
  report: {
    format: 'Letter',
    border: {
      top: '0.4in',
      right: '0.2in',
      bottom: '0.4in',
      left: '0.4in',
    },
  },
  appVersion: process.env.APP_VERSION || '1.0.14',
};
