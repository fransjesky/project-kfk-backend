'use strict';

var defaultEnvConfig = require('./default');
const pg = require('pg');
const aws = require('aws-sdk');

const awsUpload = {
  bucketName: process.env.AWS_BUCKET_NAME || 'project-xcidic4.0-bucket',
  acl: process.env.AWS_ACL || 'public-read',
  secretAccessKey:
    process.env.AWS_SECRET_ACCESS_KEY ||
    'gneA/yMKYgywroji+wB96nJU/cGYj6MF3ho0UN7n',
  accessKeyId: process.env.AWS_ACCESS_ID || 'AKIAURLA5RLTPEAP3YZJ',
  region: process.env.AWS_REGION || 'ap-southeast-1',
  environment: process.env.STORAGE_ENV || 'development',
};

aws.config.update({
  secretAccessKey: awsUpload.secretAccessKey, // Not working key, Your SECRET ACCESS KEY from AWS should go here, never share it!!!
  accessKeyId: awsUpload.accessKeyId, // Not working key, Your ACCESS KEY ID from AWS should go here, never share it!!!
  region: awsUpload.region, // region of your bucket
});

const s3 = new aws.S3();
awsUpload.s3 = s3;

module.exports = {
  db: {
    uri:
      process.env.DB_URI ||
      'postgres://website_xcidic:Xcidic2022!@websitexcidic.cr7xadlx4pbn.ap-southeast-1.rds.amazonaws.com:5432/db_xcidic_website_staging',
    options: {
      logging: '',
      host:
        process.env.DB_HOST ||
        'websitexcidic.cr7xadlx4pbn.ap-southeast-1.rds.amazonaws.com',
      port: process.env.DB_PORT || '5432',
      database: process.env.DB_NAME || 'db_xcidic_website_staging',
      username: process.env.DB_USER || 'website_xcidic',
      password: process.env.PASS || 'Xcidic2022!',
      pg: (pg.defaults.ssl = true),
    },
    // Enable sequelize debug mode
    sync: {
      force: process.env.DB_FORCE === 'true',
      // force: process.env.DB_FORCE = 'true' === 'true'
    },
  },
  log: {
    // logging with Morgan - https://github.com/expressjs/morgan
    // Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
    format: 'dev',
    fileLogger: {
      directoryPath: process.cwd(),
      fileName: 'app.log',
      maxsize: 10485760,
      maxFiles: 2,
      json: false,
    },
  },
  globalVars: {
    CREDIT_TYPE_TOPUP: 'topup',
    CREDIT_TYPE_WITHDRAWAL: 'withdrawal',
    CREDIT_TYPE_REFUND: 'refund',
    CREDIT_TYPE_PENALTY: 'penalty',
    CREDIT_TYPE_ONETIMEPAYMENT: 'onetimepayment',
  },
  app: {
    title: defaultEnvConfig.app.title + ' - Development Environment',
  },
  facebook: {
    clientID: process.env.FACEBOOK_ID || 'APP_ID',
    clientSecret: process.env.FACEBOOK_SECRET || 'APP_SECRET',
    callbackURL: '/api/auth/facebook/callback',
  },
  twitter: {
    username: '@TWITTER_USERNAME',
    clientID: process.env.TWITTER_KEY || 'CONSUMER_KEY',
    clientSecret: process.env.TWITTER_SECRET || 'CONSUMER_SECRET',
    callbackURL: '/api/auth/twitter/callback',
  },
  google: {
    clientID: process.env.GOOGLE_ID || 'APP_ID',
    clientSecret: process.env.GOOGLE_SECRET || 'APP_SECRET',
    callbackURL: '/api/auth/google/callback',
  },
  linkedin: {
    clientID: process.env.LINKEDIN_ID || 'APP_ID',
    clientSecret: process.env.LINKEDIN_SECRET || 'APP_SECRET',
    callbackURL: '/api/auth/linkedin/callback',
  },
  github: {
    clientID: process.env.GITHUB_ID || 'APP_ID',
    clientSecret: process.env.GITHUB_SECRET || 'APP_SECRET',
    callbackURL: '/api/auth/github/callback',
  },
  paypal: {
    clientID: process.env.PAYPAL_ID || 'CLIENT_ID',
    clientSecret: process.env.PAYPAL_SECRET || 'CLIENT_SECRET',
    callbackURL: '/api/auth/paypal/callback',
    sandbox: true,
  },

  mailer: {
    from: process.env.MAILER_FROM || 'agungprasetio@xcidic.com',
    emailTo: process.env.EMAIL || 'agungprasetio@xcidic.com',
    sendGridApiKey:
      process.env.SENDGRID_API_KEY ||
      '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',
  },
  livereload: true,
  seed: {
    data: {},
    init: process.env.DB_SEED === 'true',
    logging: process.env.DB_SEED_LOGGING === 'false',
  },
  uploads: {
    // Storage can be 'local' or 's3'
    storage: process.env.UPLOADS_STORAGE || 'local',
    profile: {
      image: {
        dest: './modules/users/client/img/profile/uploads/',
        limits: {
          fileSize: 1 * 1024 * 1024, // Max file size in bytes (1 MB)
        },
      },
    },
    importUpload: {
      dest: './uploads', // Compliance upload destination path
      limits: {
        fileSize: 10 * 1024 * 1024, // Max file size in bytes (10 MB)
      },
    },
    awsUpload,
  },
};
