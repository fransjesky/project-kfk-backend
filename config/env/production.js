'use strict';

var defaultEnvConfig = require('./default');
const pg = require('pg');
const aws = require('aws-sdk');

const awsUpload = {
  bucketName: process.env.AWS_BUCKET_NAME || 'kbri-lima-website-bucket',
  acl: process.env.AWS_ACL || 'public-read',
  secretAccessKey:
    process.env.AWS_SECRET_ACCESS_KEY ||
    'hP5hDAGIBpFP3e6wRkstEsQnDIOJcV+q3rW9kflZ',
  accessKeyId: process.env.AWS_ACCESS_ID || 'AKIA3DM5PFHGFFXPJ6RK',
  region: process.env.AWS_REGION || 'sa-east-1',
  environment: process.env.STORAGE_ENV || 'production',
};

aws.config.update({
  secretAccessKey: awsUpload.secretAccessKey, // Not working key, Your SECRET ACCESS KEY from AWS should go here, never share it!!!
  accessKeyId: awsUpload.accessKeyId, // Not working key, Your ACCESS KEY ID from AWS should go here, never share it!!!
  region: awsUpload.region, // region of your bucket
});

const s3 = new aws.S3();
awsUpload.s3 = s3;

module.exports = {
  db: {
    uri:
      process.env.DB_URI ||
      'postgres://kbri_lima:KBRILIMA2021!@kbri-lima-postgres-prod.c8izlnrosp45.sa-east-1.rds.amazonaws.com:5432/kbri_lima_staging',
    options: {
      logging: '',
      host:
        process.env.DB_HOST ||
        'kbri-lima-postgres-prod.c8izlnrosp45.sa-east-1.rds.amazonaws.com',
      port: process.env.DB_PORT || '5432',
      database: process.env.DB_NAME || 'kbri_lima_staging',
      username: process.env.DB_USER || 'kbri_lima',
      password: process.env.PASS || 'KBRILIMA2021!',
      pg: (pg.defaults.ssl = true),
    },
    // Enable sequelize debug mode
    sync: {
      force: process.env.DB_FORCE === 'true',
      // force: process.env.DB_FORCE = 'true' === 'true'
    },
  },
  log: {
    // logging with Morgan - https://github.com/expressjs/morgan
    // Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
    format: 'dev',
    fileLogger: {
      directoryPath: process.cwd(),
      fileName: 'app.log',
      maxsize: 10485760,
      maxFiles: 2,
      json: false,
    },
  },
  globalVars: {
    CREDIT_TYPE_TOPUP: 'topup',
    CREDIT_TYPE_WITHDRAWAL: 'withdrawal',
    CREDIT_TYPE_REFUND: 'refund',
    CREDIT_TYPE_PENALTY: 'penalty',
    CREDIT_TYPE_ONETIMEPAYMENT: 'onetimepayment',
  },
  app: {
    title: defaultEnvConfig.app.title + ' - Production Environment',
  },
  facebook: {
    clientID: process.env.FACEBOOK_ID || 'APP_ID',
    clientSecret: process.env.FACEBOOK_SECRET || 'APP_SECRET',
    callbackURL: '/api/auth/facebook/callback',
  },
  twitter: {
    username: '@TWITTER_USERNAME',
    clientID: process.env.TWITTER_KEY || 'CONSUMER_KEY',
    clientSecret: process.env.TWITTER_SECRET || 'CONSUMER_SECRET',
    callbackURL: '/api/auth/twitter/callback',
  },
  google: {
    clientID: process.env.GOOGLE_ID || 'APP_ID',
    clientSecret: process.env.GOOGLE_SECRET || 'APP_SECRET',
    callbackURL: '/api/auth/google/callback',
  },
  linkedin: {
    clientID: process.env.LINKEDIN_ID || 'APP_ID',
    clientSecret: process.env.LINKEDIN_SECRET || 'APP_SECRET',
    callbackURL: '/api/auth/linkedin/callback',
  },
  github: {
    clientID: process.env.GITHUB_ID || 'APP_ID',
    clientSecret: process.env.GITHUB_SECRET || 'APP_SECRET',
    callbackURL: '/api/auth/github/callback',
  },
  paypal: {
    clientID: process.env.PAYPAL_ID || 'CLIENT_ID',
    clientSecret: process.env.PAYPAL_SECRET || 'CLIENT_SECRET',
    callbackURL: '/api/auth/paypal/callback',
    sandbox: true,
  },
  emailTo: process.env.EMAIL || 'feedback.kbri-lima@gmail.com',
  mailer: {
    from: process.env.MAILER_FROM || 'admin@embajadaindonesia.pe',
    defaultSender: process.env.DEFAULT_FROM || 'admin@embajadaindonesia.pe',
    options: {
      host: process.env.MAILER_SERVICE_PROVIDER || 'smtp.sendgrid.net',
      port: 587,
      debug: true,
      secure: false,
      auth: {
        user: process.env.MAILER_EMAIL_ID || 'apikey',
        pass:
          process.env.MAILER_PASSWORD ||
          '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',
      },
      tls: {
        rejectUnauthorized: false,
      },
    },
  },
  livereload: true,
  seed: {
    data: {},
    init: process.env.DB_SEED === 'true',
    logging: process.env.DB_SEED_LOGGING === 'false',
  },
  uploads: {
    // Storage can be 'local' or 's3'
    storage: process.env.UPLOADS_STORAGE || 'local',
    profile: {
      image: {
        dest: './modules/users/client/img/profile/uploads/',
        limits: {
          fileSize: 1 * 1024 * 1024, // Max file size in bytes (1 MB)
        },
      },
    },
    importUpload: {
      dest: './uploads', // Compliance upload destination path
      limits: {
        fileSize: 10 * 1024 * 1024, // Max file size in bytes (10 MB)
      },
    },
    awsUpload,
  },
};
