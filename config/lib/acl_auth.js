const allowedRoles = async function (acls, routePath, method, callback) {
  try {
    const aclFound = acls.filter(function (item) {
      return item.resource === routePath && item.permission === method;
    });

    if (aclFound.length > 0) {
      callback(null, true);
    } else {
      callback(null, false);
    }
  } catch (e) {
    callback(e);
  }
};

exports.allowedRoles = allowedRoles;

exports.isAllowed = function (req, res, next) {
  if (req.user) {
    const acls = req.user.acls;

    allowedRoles(
      acls,
      req.route.path,
      req.method.toLowerCase(),
      function (err, isAllowed) {
        if (err) {
          // An authorization error occurred
          return res.status(500).send('Unexpected authorization error');
        } else {
          if (req.user) {
            if (isAllowed) {
              // Access granted! Invoke next middleware
              return next();
            } else {
              return res.status(403).json({
                message:
                  'Sorry you don’t have permissions to perform this action',
                data: null,
              });
            }
          } else {
            return res.status(401).json({
              message:
                'Your session has been expired. Please logout and re-login',
              data: null,
            });
          }
        }
      }
    );
  } else {
    // An authorization error occurred
    return res.status(500).send('Unexpected authorization error');
  }
};
