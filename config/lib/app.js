'use strict';

/**
 * Module dependencies.
 */
var config = require('../config'),
  sequelize = require('./sequelize'),
  express = require('./express'),
  chalk = require('chalk'),
  // seed = require('./seed'),
  logger = require('./logger'),
  argv = require('minimist')(process.argv.slice(2));
// const WebSocketServer = require('ws').Server;

var isDev = process.env.NODE_ENV !== 'production';
var ngrok =
  (isDev && process.env.ENABLE_TUNNEL) || argv.tunnel
    ? require('ngrok')
    : false;

module.exports.init = function init(callback) {
  sequelize.sequelize
    .sync({
      force: config.db.sync.force,
    })
    .then(function (db) {
      var app = express.init(db);
      // Seed
      // if (config.db.sync.force) {
      //   seed.setup();
      // }

      // if (config.seed.init) {
      //   console.log(chalk.bold.red('Warning:\tDB_SEED is true'));
      //   seed.start();
      // }

      if (callback) {
        callback(app, db, config);
      }

      return null;
    })
    .catch(function (error) {
      console.log(error.message);
      console.log(error.stack);
    });
};

module.exports.start = function start(callback) {
  var _this = this;

  _this.init(function (app, db, config) {
    // Start the app by listening on <port> at <host>

    //Implement basic web socket

    // let wss = new WebSocketServer({ port: 3002 });

    // wss.on('connection', function connection(ws) {
    //   console.log('Connected to a new client!');
    //   ws.send('Test websocket!');

    //   ws.on('message', function incoming(message) {
    //     console.log('received: %s', message);

    //     wss.clients.forEach(function each(client) {
    //       if (client !== ws && client.readyState === WebSocket.OPEN) {
    //         client.send(message);
    //       }
    //     });

    //   });
    // });

    app.listen(config.port, config.host, function () {
      // Connect to ngrok in dev mode
      if (ngrok) {
        ngrok.connect(config.port, function (innerErr, url) {
          if (innerErr) {
            return logger.error(innerErr);
          }

          logger.appStarted(config.port, config.host, config.db, url);
        });
      } else {
        // Create server URL
        var server =
          (process.env.NODE_ENV === 'secure' ? 'https://' : 'http://') +
          config.host +
          ':' +
          config.port;
        // Logging initialization
        console.log('--');
        console.log(chalk.green(config.app.title));
        console.log();
        console.log(chalk.green('Environment:     ' + process.env.NODE_ENV));
        console.log(chalk.green('Server:          ' + server));
        console.log(chalk.green('Database:        ' + config.db.uri));
        console.log(chalk.green('App version:     ' + config.meanjs.version));
        if (config.meanjs['meanjs-version'])
          console.log(
            chalk.green('MEAN.JS version: ' + config.meanjs['meanjs-version'])
          );
        console.log('--');

        if (callback) callback(app, db, config);
      }
    });
  });
};
