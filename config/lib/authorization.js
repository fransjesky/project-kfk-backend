'use strict';

const config = require('../config');
const jwt = require('jsonwebtoken');

var auth = {
  signToken,
  signRefreshToken,
  verifyRefreshToken,
};

// Export the token auth service
module.exports = auth;

// Sign the Token
function signToken(user, options) {
  var payload, token, jwtOptions;
  if (!user) {
    return null;
  }
  options = options || {};

  payload = {
    user: user,
  };
  jwtOptions = Object.assign(config.jwt.options, options);
  token = jwt.sign(payload, config.jwt.secret, jwtOptions);

  return token;
}

function signRefreshToken(user, options) {
  var payload, token, jwtOptions;
  if (!user) {
    return null;
  }
  options = options || {};

  payload = {
    user: user,
  };
  jwtOptions = Object.assign(config.jwt.refreshOptions, options);
  token = jwt.sign(payload, config.jwt.refreshTokenSecret, jwtOptions);

  return token;
}

//verify refresh token
function verifyRefreshToken(refreshToken) {
  return new Promise((resolve, reject) => {
    jwt.verify(refreshToken, config.jwt.refreshTokenSecret, (err, payload) => {
      if (err) return reject(err);
      const user = payload.user;
      resolve(user);
    });
  });
}
