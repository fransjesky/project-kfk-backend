'use strict';

const e = require('connect-flash');
const lodash = require('lodash');
const { kbriLogo } = require('../assets/KBRI-logo'); // get base64 KBRI logo

//function for create sheet
exports.createSheet = function (wb, datas, filter) {
  const ws = wb.addWorksheet(`${filter.data} - ${filter.year_start}`);
  const logo = wb.addImage({
    base64: kbriLogo,
    extension: 'png',
  }); // set logo to sheet
  ws.addImage(logo, {
    tl: { col: 0.15, row: 0.25 },
    ext: { width: 72.566929134, height: 88.062992126 },
  }); // add logo to sheet
  const countryType = ws.getCell('C1');
  countryType.value = `${filter.country.toUpperCase()} - ${filter.type.toUpperCase()}`; // add country and type to sheet
  const destinationCountry = ws.getCell('C2');
  destinationCountry.value = '[Destination Country] INDONESIA'; // add destination country to sheet
  const period = ws.getCell('C3');
  period.value = `Period: FROM JAN-${filter.year_start} TO DEC-${filter.year_start}`; // add period to sheet
  ws.addRow([]); // menambahkan row kosong
  const rows = ws.getRows(1, 5);
  // set header rows to remove border
  rows.map((row, index) => {
    for (let i = 1; i <= 17; i++) {
      if (index + 1 == 5) {
        row.getCell(i).border = {
          top: { style: 'thin', color: { argb: 'FFFFFF' } },
          left: { style: 'thin', color: { argb: 'FFFFFF' } },
          right: { style: 'thin', color: { argb: 'FFFFFF' } },
        };
      } else {
        row.getCell(i).border = {
          top: { style: 'thin', color: { argb: 'FFFFFF' } },
          left: { style: 'thin', color: { argb: 'FFFFFF' } },
          bottom: { style: 'thin', color: { argb: 'FFFFFF' } },
          right: { style: 'thin', color: { argb: 'FFFFFF' } },
        };
      }
    }
  });
  // get header column
  const headers = Object.keys(datas[0].dataValues)
    .map((val, indx) => setHeader(val, filter.data, indx))
    .filter((header) => header);
  const rowHeaders = ws.getRow(6);
  headers.map((header, index) => {
    // set header column
    rowHeaders.getCell(index + 1).value = header.header;
    ws.getColumn(index + 1).width = header.width;
    ws.getColumn(index + 1).key = header.key;
  });
  const autofil = `${ws.lastRow._cells[0]._address}:${
    ws.lastRow._cells[headers.length - 1]._address
  }`; // set auto fill to headers column
  ws.autoFilter = autofil;

  ws.getRow(6).eachCell({ includeEmpty: true }, (cell, cellNumber) => {
    // styling all data cell
    ws.getRow(6).height = 32.1;
    ws.getRow(6).fill = {
      type: 'pattern',
      pattern: 'solid',
      fgColor: { argb: '30A6E5' },
    };
    cell.alignment = { vertical: 'middle', horizontal: 'center' };
    cell.font = {
      name: 'Calibri',
      color: { argb: 'FFFFFF' },
      bold: true,
      size: 11,
    };
  });
  // membuat data baru untuk menambahkan ke sheet
  const newDatas = datas.map((data) => {
    return {
      ...data.dataValues,
      hsNumber: +data.hsNumber,
    };
  });
  // add row with new data
  newDatas.forEach((data) => ws.addRow(data).commit());
  let totalData = {
    [headers[0].key]: 'Total',
  };
  // create row for Total
  let temp = headers.findIndex((el) => el.key == 'totalRecords');
  for (let x = temp; x < headers.length; x++) {
    let cellTd = headers[x].key;
    if (cellTd === 'uskg') {
      totalData[cellTd] = +(totalData.totalUs / totalData.totalKg).toFixed(3);
    } else if (cellTd == 'percentage') {
      totalData[cellTd] =
        newDatas
          .map((item) => item[cellTd].slice(0, item[cellTd].indexOf('%')))
          .reduce((prev, curr) => +prev + +curr, 0)
          .toFixed(2) + '%';
    } else {
      totalData[cellTd] = newDatas
        .map((item) => item[cellTd])
        .reduce((prev, curr) => prev + curr, 0);
    }
  }
  // add row total data
  ws.addRow(totalData).commit();
  ws.views = [{ state: 'frozen', ySplit: 6, activeCell: 'A1' }];
  ws.lastRow.fill = {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '30A6E5' },
  };
  ws.lastRow.font = {
    name: 'Calibri',
    color: { argb: 'FFFFFF' },
    bold: true,
    size: 11,
  };
  return ws;
};

// function to set header column sheets
function setHeader(val, data, index) {
  if (index === 0 && data !== 'Products') {
    return {
      header: lodash.startCase(val),
      key: val,
      width: 60,
    };
  } else if (val == 'htsCodeDesc') {
    return {
      header: 'HTS Code Description',
      key: val,
      width: 99.89,
    };
  } else if (val == 'totalUs') {
    return {
      header: 'Total US$ FOBTot',
      key: val,
      width: 'Total US$ FOBTot'.length + 5,
    };
  } else if (val == 'uskg') {
    return {
      header: 'US$ / kg',
      key: val,
      width: 'US$ / kg'.length + 5,
    };
  } else if (val == 'hsNumber') {
    return {
      header: 'HS Number',
      key: val,
      width: 'HS Number'.length + 5,
    };
  } else if (val == 'percentage') {
    return {
      header: '%',
      key: val,
      width: val.length,
    };
  } else if (val == 'year' || val == 'country' || val == 'type') {
    return null;
  } else {
    return {
      header: lodash.startCase(val),
      key: val,
      width: lodash.startCase(val).length + 5,
    };
  }
}
