const sharp = require('sharp');
const Rx = require('rxjs');
const { map, mergeMap, toArray } = require('rxjs/operators');
const config = require('../config');

class S3Storage {
  constructor(options) {
    options.bucket =
      options.bucket || process.env.AWS_BUCKET || 'project-xcidic4.0-bucket';
    options.acl = options.acl || process.env.AWS_ACL || 'public-read';
    options.s3 = options.s3 || config.uploads.awsUpload.s3;

    if (!options.bucket) {
      throw new Error('You have to specify bucket for AWS S3 to work.');
    }

    this.options = options;
  }

  handleFile(sizes, imageBuffer, cb) {
    const opts = this.options;
    if (
      !imageBuffer &&
      !imageBuffer.data &&
      !Buffer.isBuffer(imageBuffer.data)
    ) {
      cb('Path must be a Buffer'); // eslint-disable-line
      return;
    }

    if (!sizes && !Array.isArray(sizes) && sizes.length === 0) {
      cb('sizes must be Array'); // eslint-disable-line
      return;
    }

    const imagetype = imageBuffer.type;
    const rules = Rx.from(sizes);
    if (imagetype.includes('svg')) {
      const params = {
        Bucket: opts.bucket,
        Key: opts.key,
        ACL: opts.acl,
        CacheControl: opts.cacheControl,
        ContentType: opts.contentType || imagetype,
        Metadata: opts.metadata,
        StorageClass: opts.storageClass,
        ServerSideEncryption: opts.serverSideEncryption,
        SSEKMSKeyId: opts.sseKmsKeyId,
        Body: imageBuffer.data,
      };
      const upload = opts.s3.upload(params);
      const promise = this._awsPromise({ path: 'original' }, upload);
      Rx.from(promise).subscribe(function (res) {
        cb(null, [res]);
      }, cb);
    } else {
      const resizeImage = (size) => {
        if (size.path === 'original') {
          size.Body = sharp(imageBuffer.data);
        } else {
          size.Body = sharp(imageBuffer.data).resize(size.xy);
        }
        return size;
      };
      const eachUpload = (size) => {
        const params = {
          Bucket: opts.bucket,
          Key: `${opts.key}-${size.path}`,
          ACL: opts.acl,
          CacheControl: opts.cacheControl,
          ContentType: opts.contentType || imagetype,
          Metadata: opts.metadata,
          StorageClass: opts.storageClass,
          ServerSideEncryption: opts.serverSideEncryption,
          SSEKMSKeyId: opts.sseKmsKeyId,
          Body: size.Body,
        };
        const upload = opts.s3.upload(params);
        const promise = this._awsPromise(size, upload);
        return Rx.from(promise);
      };
      const resizeWithSharp = map(resizeImage);
      const uploadToAws = mergeMap(eachUpload);
      rules
        .pipe(resizeWithSharp, uploadToAws, toArray())
        .subscribe(function (res) {
          cb(null, res);
        }, cb);
    }
  }

  _awsPromise(size, upload) {
    let currentSize = 0;
    return new Promise((resolve, reject) => {
      upload.on('httpUploadProgress', function (ev) {
        if (ev.total) currentSize = ev.total;
      });
      upload.promise().then((result) => {
        resolve({
          ...result,
          currentSize,
          path: size.path,
        });
      }, reject);
    });
  }
}

function transformerAndS3Uploader(options) {
  return new S3Storage(options);
}

module.exports = transformerAndS3Uploader;
