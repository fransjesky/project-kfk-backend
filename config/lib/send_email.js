'use strict';

const path = require('path');
const sgMail = require('@sendgrid/mail');
const config = require(path.resolve('./config/config'));

sgMail.setApiKey(config.mailer.sendGridApiKey);

exports.sendEmail = async function (emailOption) {
  await sgMail.send(emailOption);
};
