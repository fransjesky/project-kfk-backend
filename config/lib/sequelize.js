'use strict';

var config = require('../config'),
  fs = require('fs'),
  path = require('path'),
  Sequelize = require('sequelize');

var db = {};

// Sequelize
var sequelize = new Sequelize(config.db.uri, {
  logging: false,
  dialect: 'postgres',
  sync: { force: true },
  operatorsAliases: 0,
  pool: {
    max: 30,
    min: 0,
    acquire: 20000,
    idle: 20000,
  },
  dialectOptions: {
    ssl: {
      require: true,
      rejectUnauthorized: false,
    },
  },
});

// Import models
config.files.models.forEach(function (modelPath) {
  var model = require(path.resolve(modelPath))(sequelize, Sequelize.DataTypes);
  // var model = sequelize.import(path.resolve(modelPath));
  db[model.name] = model;
});

// Associate models
Object.keys(db).forEach(function (modelName) {
  if ('associate' in db[modelName]) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
