'use strict';

// Load the module dependencies
var config = require('../config'),
  path = require('path'),
  fs = require('fs'),
  http = require('http'),
  https = require('https'),
  passport = require('passport'),
  socketio = require('socket.io'),
  jwt = require('jsonwebtoken');

// Define the Socket.io configuration method
module.exports = function (app, db) {
  let server;
  if (config.secure && config.secure.ssl === true) {
    // Load SSL key and certificate
    var privateKey = fs.readFileSync(
      path.resolve(config.secure.privateKey),
      'utf8'
    );
    var certificate = fs.readFileSync(
      path.resolve(config.secure.certificate),
      'utf8'
    );
    var caBundle;

    try {
      caBundle = fs.readFileSync(path.resolve(config.secure.caBundle), 'utf8');
    } catch (err) {
      console.log("Warning: couldn't find or read caBundle file");
    }

    var options = {
      key: privateKey,
      // cert: certificate,
      ca: caBundle,
      //  requestCert : true,
      //  rejectUnauthorized : true,
      secureProtocol: 'TLSv1_method',
      ciphers: [
        'ECDHE-RSA-AES128-GCM-SHA256',
        'ECDHE-ECDSA-AES128-GCM-SHA256',
        'ECDHE-RSA-AES256-GCM-SHA384',
        'ECDHE-ECDSA-AES256-GCM-SHA384',
        'DHE-RSA-AES128-GCM-SHA256',
        'ECDHE-RSA-AES128-SHA256',
        'DHE-RSA-AES128-SHA256',
        'ECDHE-RSA-AES256-SHA384',
        'DHE-RSA-AES256-SHA384',
        'ECDHE-RSA-AES256-SHA256',
        'DHE-RSA-AES256-SHA256',
        'HIGH',
        '!aNULL',
        '!eNULL',
        '!EXPORT',
        '!DES',
        '!RC4',
        '!MD5',
        '!PSK',
        '!SRP',
        '!CAMELLIA',
      ].join(':'),
      honorCipherOrder: true,
    };
    // console.log("USE HTTPS");
    // Create new HTTPS Server
    server = require('https').createServer(options, app);
    // console.log('SERVER HTTPS', server);
  } else {
    // console.log("USE HTTP");
    // Create a new HTTP server
    server = require('http').createServer(app);
    // console.log("SERVER HTTP", server);
  }
  server = require('http').createServer(app);
  // Create a new Socket.io server
  // server = require('http').createServer(app);
  // const io = require('socket.io')(server);
  // const io = require("socket.io")(server, {
  //   origins: ["http://localhost:3001", "https://dev-api.fitcells.com"]
  // });
  // let io = socketio(server, {
  //   pingTimeout: 5000,
  //   pingInterval: 5000,
  //   origins: '*:*',
  //   transports: ['websocket'],
  // });
  const io = require('socket.io')(server, {
    cors: {
      origin: '*',
      credentials: true,
    },
    transports: ['polling', 'websocket'],
    allowEIO3: true,
  });
  // set socket to req data
  app.set('socketio', { io });

  // Intercept Socket.io's handshake request
  io.use(function (socket, next) {
    var token = socket.handshake.headers.authorization;
    // console.log('TOKEN SOCKET', token);
    // Use Passport to populate the user details
    if (token) {
      // console.log("TOKEN VALUE", token.token)
      jwt.verify(token, config.jwt.secret, function (err, decode) {
        // console.log("DECODE TOKEN", decode)
        // console.log("ERROR DECODE", err);
        if (err) return next(new Error('Token is invalid'));
        if (decode.exp) {
          if (decode.exp * 1000 >= Date.now()) {
            socket.request.user = decode;
            return next();
          }
          return next(new Error('Token is expired'));
        }
      });
    } else {
      next(new Error('Token is not provided'));
    }
    passport.initialize()(socket.request, {}, function () {
      passport.authenticate('jwt', { session: false }, function (err, user) {
        if (err) {
          return next(new Error(err));
        }
        if (user) {
          socket.request.user = user;
          // console.log(user)
        } else {
          // console.log("USER SOCKET DOESNT EXIST");
        }

        next();
      })(socket.request, socket.request.res, next);
    });
  });
  // Add an event listener to the 'connection' event
  // function startPingAll() {
  //   // call this function just once
  //   setInterval(() => {
  //     io.of('/').sockets.forEach((socket) => {
  //       socket.emit('xping', (response) => {
  //         console.log(response);
  //       });
  //     });
  //   }, 10000); // every 10 seconds
  // }

  // startPingAll();

  io.on('connection', function (socket) {
    // console.log('SocketIO is connected');
    // change socket to req data
    app.set('socketio', { io, socket });
    socket.removeAllListeners();
    // io.emit('success-connect', socket);
    socket.on('error', (err) => console.log('ERR CONNECT ERR', err));
    socket.on('connect_failed', (err) =>
      console.log('ERR CONNECT FAILED', err)
    );
    socket.on('disconnect', (err) => console.log('ERR DISCONNECT', err));
    config.files.sockets.forEach(function (socketConfiguration) {
      require(path.resolve(socketConfiguration))(io, socket);
    });
  });

  io.on('error', function (error) {
    console.log('error socket ->', error.message);
    console.log('error stack socket ->', error.stack);
  });

  return server;
};
