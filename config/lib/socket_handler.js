let path = require('path'),
  config = require(path.resolve('./config/config')),
  db = require(path.resolve('./config/lib/sequelize')),
  Acl = db.Acl,
  AclRole = db.AclRole,
  Notifications = db.Notifications,
  // socket = require('socket.io-client')(config.domain);
  socket = require('socket.io-client')('ws://15.228.10.0:3001', {
    transports: ['polling', 'websocket'],
  });

exports.sendSocket = function (moduleName, sendData) {
  return new Promise(async function (resolve, reject) {
    try {
      let acl = await Acl.findOne({
        where: {
          module: moduleName,
        },
      });
      let aclId = acl._id;
      let aclRoles = await AclRole.findAll({
        where: {
          aclId: aclId,
        },
        raw: true,
      });
      aclRoles.forEach((item) => {
        let data = {
          roleId: item.roleId,
          data: sendData,
        };
        socket.emit('notifByRole', data); // send roleId
      });
      resolve();
    } catch (e) {
      reject(e);
    }
  });
};

// emiting socket
exports.sendNotif = ({ title, content, userId }) => {
  return new Promise(async (resolve, reject) => {
    try {
      const unread = await Notifications.count({
        where: { user: userId, isRead: false },
      });
      // socket.on('connect_error', (err) => {
      //   console.log(`connect_error due to ${err.message}`);
      // });
      socket.emit('join', { userId });
      socket.emit('send-notif', { title, content, unread });
      resolve();
    } catch (e) {
      reject(e);
    }
  });
};
