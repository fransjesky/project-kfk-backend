const { createHook, executionAsyncId } = require('async_hooks');

module.exports = tracker;

function tracker() {
  let ctx = {};
  const asyncHooks = createHook({
    init(asyncId, type, triggerAsyncId, resource) {
      if (ctx[triggerAsyncId]) {
        ctx[asyncId] = ctx[triggerAsyncId];
      }
    },
    destroy(asyncId) {
      delete ctx[asyncId];
    },
  });

  asyncHooks.enable();

  process.on('uncaughtException', function (err) {
    const id = executionAsyncId();
    console.log(`path=${ctx[id]}\n`);
    console.dir(err);
    console.log(err.stack);
  });

  return function (req, res, next) {
    const id = executionAsyncId();
    ctx[id] = req;
    next();
  };
}
