const multer = require('multer');
const path = require('path');
const config = require(path.resolve('./config/config'));
const s3Storage = require('multer-sharp-s3');

const uploadFile = (req, res, next) => {
  const storage = s3Storage({
    Bucket: config.uploads.awsUpload.bucketName,
    Key: `${
      config.uploads.awsUpload.environment
    }/attachments/${Date.now()}-attachment`,
    Metadata: {
      'Content-Encoding': 'gzip',
    },
    ACL: config.uploads.awsUpload.acl,
    s3: config.uploads.awsUpload.s3,
  });

  const imageFileFilter = function (req, file, callback) {
    if (
      file.mimetype !== 'image/png' &&
      file.mimetype !== 'image/jpg' &&
      file.mimetype !== 'image/jpeg'
    ) {
      const error = new Error();
      error.code = 'UNSUPPORTED_MEDIA_TYPE';
      return callback(error, false);
    }
    callback(null, true);
  };

  const upload = multer({
    storage: storage,
    fileFilter: imageFileFilter,
    limits: {
      fileSize: 1 * 1024 * 1024,
    },
  }).single('image');

  upload(req, res, (error) => {
    if (error) {
      if (error.code == 'UNSUPPORTED_MEDIA_TYPE') {
        return res.status(500).json({
          message: `Sorry, this file type is not supported. Allowed type: PNG, JPEG, JPG`,
        });
      } else {
        return res.status(500).json({
          message: 'Sorry, your file exceeds 1.0MB',
        });
      }
    }
    next();
  });
};

module.exports = uploadFile;
