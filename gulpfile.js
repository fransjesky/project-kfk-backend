'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
  defaultAssets = require('./config/assets/default'),
  gulp = require('gulp'),
  gulpLoadPlugins = require('gulp-load-plugins'),
  plugins = gulpLoadPlugins({}),
  semver = require('semver');

// Set NODE_ENV to 'development'
gulp.task('env:dev', function (done) {
  process.env.NODE_ENV = 'development';
  return done();
});

// Set NODE_ENV to 'production'
gulp.task('env:prod', function (done) {
  process.env.NODE_ENV = 'production';
  return done();
});

// Nodemon task
gulp.task('nodemon', function (done) {
  // Node.js v7 and newer use different debug argument
  var debugArgument = semver.satisfies(process.versions.node, '>=7.0.0')
    ? '--inspect'
    : '--debug';

  return plugins
    .nodemon({
      script: 'server.js',
      nodeArgs: [debugArgument],
      ext: 'js,html',
      verbose: true,
      watch: _.union(defaultAssets.allJS, defaultAssets.config),
    })
    .on('start', done);
});

// Nodemon task without verbosity or debugging
gulp.task('nodemon-nodebug', function (done) {
  return plugins
    .nodemon({
      script: 'server.js',
      ext: 'js,html',
      watch: _.union(defaultAssets.allJS, defaultAssets.config),
    })
    .on('start', done);
});

// // Run the project in development mode with node debugger enabled
gulp.task('default', gulp.series(gulp.parallel('nodemon')));

// Run the project in production mode
gulp.task('prod', gulp.series('env:prod', gulp.parallel('nodemon-nodebug')));
