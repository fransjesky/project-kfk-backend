'use strict';

/**
 * Module dependencies
 */
const path = require('path'),
  db = require(path.resolve('./config/lib/sequelize')),
  errorHandler = require(path.resolve(
    './modules/core/controllers/errors.server.controller'
  )),
  { Op } = require('sequelize');

/**
 * Create an aclRole
 */
exports.create = async function (req, res) {
  const acl = req.body;
  let aclData = acl.actions.map(function (item) {
    return {
      module: req.body.module,
      action: item.action,
      resource: item.resource,
      permission: item.permission,
      userId: req.user._id,
    };
  });
  try {
    const aclResult = await db.Acl.bulkCreate(aclData);
    res.json({
      message: 'Acl has been created',
      data: aclResult,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * Show the current aclRole
 */
exports.read = async function (req, res) {
  let _id = req.params.aclRoleId;
  try {
    const acl = await db.Acl.findOne({
      where: {
        _id: _id,
      },
    });
    if (!acl) {
      return res.status(400).send({
        message: 'Failed to load acl id ' + _id,
      });
    }
    res.json(acl);
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * Update an aclRole
 */
exports.update = async function (req, res) {
  let _id = req.params.aclRoleId;
  let acl = req.body;
  try {
    const aclResult = await db.Acl.update(acl, { where: { _id: _id } });
    res.json({
      message: 'Acl has been updated',
      data: acl,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * Delete an aclRole
 */
exports.delete = async function (req, res) {
  let _id = req.params.aclRoleId;
  try {
    const acl = await db.Acl.destroy({
      where: {
        _id: _id,
      },
    });
    res.json({
      message: 'Acl has been deleted',
      data: acl,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * Bulk Delete an masterMenu
 */
exports.bulkDelete = async function (req, res) {
  let ids = req.body.ids;
  if (ids.length > 0) {
    try {
      let aclDeleted = await db.Acl.destroy({
        where: { _id: { [Op.in]: ids } },
      });
      res.json({
        message: aclDeleted + ' acl has been deleted',
        data: aclDeleted,
      });
    } catch (e) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(e),
      });
    }
  }
};
/**
 * List of AclRoles
 */
exports.list = async function (req, res) {
  let searchCriteria = {};
  let joinCreatedBy = {
    association: 'User',
    attributes: ['_id', 'displayName'],
  };
  if (req.query.advanceSearch) {
    searchCriteria = { [Op.and]: [] };
    if (req.query.module) {
      searchCriteria[Op.and].push({
        module: { [Op.iLike]: '%' + req.query.module + '%' },
      });
    }
    if (req.query.action) {
      searchCriteria[Op.and].push({
        action: { [Op.iLike]: '%' + req.query.action + '%' },
      });
    }
    if (req.query.resource) {
      searchCriteria[Op.and].push({
        resource: { [Op.iLike]: '%' + req.query.resource + '%' },
      });
    }
    if (req.query.permission) {
      searchCriteria[Op.and].push({
        permission: { [Op.iLike]: '%' + req.query.permission + '%' },
      });
    }
    if (req.query.displayName) {
      joinCreatedBy.where = {
        displayName: { [Op.iLike]: '%' + req.query.displayName + '%' },
      };
    }
    if (searchCriteria[Op.and] && searchCriteria[Op.and].length === 0) {
      searchCriteria = {};
    }
  }
  try {
    let orderBy =
      req.query.order && req.query.orderMethod
        ? [req.query.order, req.query.orderMethod]
        : ['createdAt', 'ASC'];
    const acls = await db.Acl.findAndCountAll({
      where: searchCriteria,
      include: [joinCreatedBy],
      order: [orderBy],
      offset: req.query.skip || 0,
      limit: req.query.limit || 100,
    });
    res.json({ count: acls.count, data: acls.rows });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};
