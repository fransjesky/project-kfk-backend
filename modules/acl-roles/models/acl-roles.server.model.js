'use strict';

module.exports = (sequelize, DataTypes) => {
  var AclRole = sequelize.define('AclRole', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
  });

  return AclRole;
};
