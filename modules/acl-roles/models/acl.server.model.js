'use strict';

/**
 * Article Schema
 */
module.exports = (sequelize, DataTypes) => {
  const Acl = sequelize.define('Acl', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    module: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: null,
    },
    action: {
      type: DataTypes.TEXT,
      allowNull: false,
      defaultValue: null,
    },
    resource: {
      type: DataTypes.TEXT,
      allowNull: false,
      defaultValue: null,
    },
    permission: {
      type: DataTypes.TEXT,
      allowNull: false,
      defaultValue: null,
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true,
      defaultValue: null,
    },
  });
  Acl.associate = function (models) {
    Acl.belongsToMany(models.Role, { through: 'AclRole', foreignKey: 'aclId' });
  };
  return Acl;
};
