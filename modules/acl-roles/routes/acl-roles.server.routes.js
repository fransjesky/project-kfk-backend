'use strict';

/**
 * Module dependencies
 */
const path = require('path');
const policy = require(path.resolve('./config/lib/acl_auth'));
const aclRoles = require('../controllers/acl-roles.server.controller');

module.exports = (app) => {
  // AclRoles collection routes
  app
    .route('/api/acl-roles')
    .all(policy.isAllowed)
    .get(aclRoles.list)
    .post(aclRoles.create);

  // Bulk Delete userRole routes
  app
    .route('/api/acl-roles/bulk-delete')
    .all(policy.isAllowed)
    .post(aclRoles.bulkDelete);

  // Single aclRole routes
  app
    .route('/api/acl-roles/:aclRoleId')
    .all(policy.isAllowed)
    .get(aclRoles.read)
    .put(aclRoles.update)
    .delete(aclRoles.delete);
};
