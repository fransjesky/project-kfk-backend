'use strict';

/**
 * Module dependencies
 */
const path = require('path');
const _ = require('lodash');
const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const db = require(path.resolve('./config/lib/sequelize'));
const { Op } = require('sequelize');
const ExtractJwt = require('passport-jwt').ExtractJwt;

module.exports = (config) => {
  let opts = {
    jwtFromRequest: ExtractJwt.versionOneCompatibility({
      tokenQueryParameterName: 'auth_token',
    }),
    secretOrKey: config.jwt.secret,
  };

  passport.use(
    new JwtStrategy(opts, async function (jwt_payload, done) {
      try {
        let user = await db.User.findOne({
          where: { id: jwt_payload.user.id },
          include: [
            {
              model: db.Role,
            },
          ],
        });

        if (!user) {
          done(null, false, {
            message: 'Invalid Token',
            data: null,
          });
          return null;
        }

        const data = await db.Role.findOne({
          where: {
            id: user.Role.id,
          },
          include: [
            {
              model: db.Acl,
            },
          ],
        });

        let acls = [];
        data.Acls.forEach((item) => {
          delete item.dataValues.AclRole;
          delete item.dataValues.createdAt;
          delete item.dataValues.updatedAt;
          acls.push(item.dataValues);
        });

        acls = _.uniqBy(acls, 'id');
        user.acls = acls;

        return done(null, user);
      } catch (e) {
        console.log(e);
        return done(
          new Error('Failed to load User ' + jwt_payload.user.name),
          jwt_payload.user.name
        );
      }
    })
  );
};
