'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  _ = require('lodash'),
  db = require(path.resolve('./config/lib/sequelize')),
  { Op } = require('sequelize'),
  passport = require('passport'),
  LocalStrategy = require('passport-local').Strategy,
  config = require(path.resolve('./config/config'));

module.exports = function () {
  // Use local strategy
  passport.use(
    new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'password',
      },
      async function (email, password, done) {
        try {
          const user = await db.User.findOne({
            where: {
              email: email.toLowerCase(),
            },
            include: [
              {
                model: db.Role,
                attributes: ['id', 'name'],
              },
            ],
          });
          if (!user || !user.authenticate(user, password)) {
            done(null, false, {
              message:
                'Failed to login, your login credentials don’t match an account in our system',
              data: null,
            });
            return null;
          }

          const data = await db.Role.findOne({
            where: {
              id: user.Role.id,
            },
            include: [
              {
                model: db.MasterMenu,
                attributes: ['id', 'title', 'icon', 'path', 'parentId'],
                where: {
                  id: {
                    [Op.notIn]: [9],
                  },
                },
                required: false,
              },
            ],
          });

          let children = [];
          let parentMenu = [];

          data.MasterMenus.forEach((menu) => {
            delete menu.dataValues.RoleMenu;

            if (menu.parentId !== null) {
              children.push(menu);
            } else {
              parentMenu.push(menu);
            }
          });

          let masterMenus = [];

          parentMenu.forEach((menu) => {
            menu.dataValues.children = children.filter(
              (item) => item.parentId === menu.id
            );
            delete menu.dataValues.parentId;

            masterMenus.push(menu.dataValues);
          });

          masterMenus = _.uniqBy(parentMenu, 'id');
          user.masterMenus = masterMenus;

          return done(null, user);
        } catch (err) {
          return done(err);
        }
      }
    )
  );
};
