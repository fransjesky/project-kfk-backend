'use strict';

/**
 * Module dependencies
 */

const path = require('path');
const db = require(path.resolve('./config/lib/sequelize'));
const User = db.User;
const errorHandler = require(path.resolve(
  './modules/core/controllers/errors.server.controller'
));
const authorization = require(path.resolve('./config/lib/authorization'));
const passport = require('passport');
const _ = require('lodash');

const userLoginLogs = (headers, email) => {
  let agent = headers && headers['user-agent'];
};

/**
 * login
 */
exports.login = async function (req, res, next) {
  try {
    passport.authenticate('local', async (err, user, info) => {
      if (err || !user) {
        return res.status(400).send({ message: info });
      }

      delete user.dataValues.password;
      delete user.dataValues.salt;
      delete user.dataValues.createdAt;
      delete user.dataValues.updatedAt;
      delete user.dataValues.RoleId;
      delete user.dataValues.MasterMenus;
      const token = authorization.signToken(user);

      user.dataValues.masterMenus = user.masterMenus;
      userLoginLogs(req.headers, user.email);

      return res.status(200).json({
        message: 'You have succesfully signed in',
        data: { user, token },
      });
    })(req, res, next);
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};
