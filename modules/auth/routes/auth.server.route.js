'use strict';

/**
 * Module dependencies
 */

const authController = require('../controller/auth.server.controller');

/**
 * Routes
 */
module.exports = function (app) {
  //route for cms
  app.route('/api/auth/login').post(authController.login);
};
