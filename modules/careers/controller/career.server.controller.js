'use strict';

/**
 * Module dependencies
 */

const path = require('path');
const db = require(path.resolve('./config/lib/sequelize'));
const Career = db.Career;
const errorHandler = require(path.resolve(
  './modules/core/controllers/errors.server.controller'
));
const { Op } = require('sequelize');
const { slugify } = require('../../../config/lib/slugify');

/**
 * list of career
 */
exports.list = async function (req, res) {
  try {
    const {
      keyword,
      department,
      sort = 'createdAt',
      order = 'DESC',
      limit = 5,
      offset = 0,
    } = req.query;

    let query = {
      where: {
        [Op.and]: [],
      },
      order: [
        sort === 'department'
          ? [{ model: db.Department }, 'name', order]
          : [sort, order],
        ['departmentId', 'asc'],
      ],
      limit,
      offset,
      attributes: ['id', 'name', 'jobType', 'view', 'status', 'createdAt'],
      include: {
        model: db.Department,
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
        where: {
          [Op.and]: [],
        },
      },
    };

    if (keyword) {
      query.where[Op.and].push({
        name: { [Op.iLike]: `%${keyword}%` },
      });
    }

    if (department) {
      query.include.where[Op.and].push({
        name: { [Op.iLike]: `%${department}%` },
      });
    }

    const careers = await Career.findAndCountAll(query);
    return res.status(200).json({
      count: careers.count,
      data: careers.rows,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * List of career for main website
 */
exports.publicList = async function (req, res) {
  const { name, department, jobType } = req.query;

  let query = {
    attributes: ['id', 'name', 'logo', 'description'],
    where: {
      [Op.and]: [],
    },
    include: {
      model: Career,
      attributes: ['id', 'name', 'numberVacancy', 'jobType', 'status', 'slug'],
      where: {
        [Op.and]: [{ status: 'active' }],
      },
    },
  };

  if (department) {
    query.where[Op.and].push({ name: { [Op.iLike]: `%${department}%` } });
  }

  if (name) {
    query.include.where[Op.and].push({ name: { [Op.iLike]: `%${name}%` } });
  }

  if (jobType) {
    query.include.where[Op.and].push({ jobType });
  }

  try {
    const data = await db.Department.findAll(query);
    return res.status(200).json({
      data,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

exports.publicListByDepartment = async function (req, res) {
  const name = req.params.name;

  try {
    const department = await db.Department.findOne({
      where: { name },
      include: [
        {
          model: Career,
          attributes: [
            'id',
            'name',
            'numberVacancy',
            'jobType',
            'status',
            'slug',
          ],
          where: {
            [Op.and]: [{ status: 'active' }],
          },
        },
      ],
    });

    if (!department) {
      return res.status(404).send({
        message: `department with name ${name} is not found`,
      });
    }

    return res.status(200).json({
      data: department,
    });
  } catch (error) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(error),
    });
  }
};

/**
 * create career
 */
exports.create = async function (req, res) {
  const payload = req.body;
  try {
    const career = await Career.create(payload);
    await career.update({ slug: slugify(`${career.id} ${career.name}`) });
    return res.status(200).json({
      message: 'A new job vacancy has been Added successfully',
      data: career,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * show one career
 */
exports.read = async function (req, res) {
  const id = req.params.id;

  try {
    const career = await Career.findOne({
      where: { id },
      attributes: {
        exclude: ['departmentId', 'status'],
      },
      include: {
        model: db.Department,
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      },
    });

    //check if career not with id params not found
    if (!career) {
      return res.status(400).send({
        message: `A job vacancy with id ${id} is not found`,
      });
    }

    return res.status(200).json({
      data: career,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

exports.publicRead = async function (req, res) {
  const slug = req.params.slug;
  try {
    const career = await Career.findOne({
      where: { slug, status: 'active' },
      attributes: {
        exclude: ['departmentId', 'status', 'createdAt', 'updatedAt'],
      },
      include: {
        model: db.Department,
        attributes: ['id', 'name'],
      },
    });

    if (!career) {
      return res.status(404).send({
        message: `career with slug ${slug} is not found`,
      });
    }

    await career.update({ view: parseInt(career.view) + 1 });

    return res.status(200).json({
      data: career,
    });
  } catch (error) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(error),
    });
  }
};

/**
 * update career
 */
exports.update = async function (req, res) {
  const id = req.params.id;
  const payload = req.body;

  try {
    //query
    const career = await Career.findOne({ where: { id } });

    //check if career not with id params not found
    if (!career) {
      return res.status(400).send({
        message: `career with id ${id} is not found`,
      });
    }

    await career.update(payload);

    if (payload.status) {
      if (payload.status == 'archive') {
        return res.status(200).json({
          message: `${career.name} has been Achieved successfully`,
          data: career,
        });
      }

      if (payload.status == 'active') {
        return res.status(200).json({
          message: `${career.name} has been Showed successfully`,
          data: career,
        });
      }
    }

    return res.status(200).json({
      message: `${career.name} has been edited successfully`,
      data: career,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * delete Career
 */
exports.delete = async function (req, res) {
  const id = req.params.id;

  try {
    const career = await Career.findOne({ where: { id } });

    //check if career not with id params not found
    if (!career) {
      return res.status(400).send({
        message: `Career with id ${id} is not found`,
      });
    }

    await career.destroy();

    return res.status(200).json({
      message: `${career.name} has been deleted successfully`,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

exports.getSkiils = async function (req, res) {
  const { name, type, sort = 'createdAt', order = 'ASC' } = req.query;

  const query = {
    where: {},
    order: [[sort, order]],
    attributes: ['id', 'name'],
  };

  if (name) {
    query.where[Op.and].push({
      name: { [Op.iLike]: `%${name}%` },
    });
  }

  if (type) {
    query.where[Op.or] = [
      {
        type: type,
      },
      {
        type: 'hybrid',
      },
    ];
  }

  try {
    const skills = await db.Skill.findAll(query);

    return res.status(200).json({
      data: skills,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

exports.addSkill = async function (req, res) {
  try {
    const payload = req.body;

    const skill = await db.Skill.create(payload);

    return res.status(200).json({
      message: 'A new skill has been Added successfully',
      data: skill,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};
