'use strict';

/**
 * Careers Schema
 */

module.exports = function (sequelize, DataTypes) {
  const Careers = sequelize.define('Career', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a name',
        },
      },
    },
    numberVacancy: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a Number Vacancy',
        },
      },
    },
    jobType: {
      type: DataTypes.ENUM('Full-Time', 'Part-Time', 'Internship', 'Contract'),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a Job type',
        },
        isIn: [['Full-Time', 'Part-Time', 'Internship', 'Contract']],
      },
    },
    jobDescription: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a Description',
        },
      },
    },
    requirementDescription: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a order',
        },
      },
    },
    experienceYear: {
      type: DataTypes.STRING(5),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a year experience',
        },
      },
    },
    skillNeeded: {
      type: DataTypes.ARRAY(DataTypes.TEXT),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please insert a skill needed',
        },
      },
    },
    additionalSkillNeeded: {
      type: DataTypes.ARRAY(DataTypes.TEXT),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please insert a year experience',
        },
      },
    },
    jobStreet: {
      type: DataTypes.TEXT,
      allowNull: true,
      defaultValue: null,
    },
    glints: {
      type: DataTypes.TEXT,
      allowNull: true,
      defaultValue: null,
    },
    techinAsia: {
      type: DataTypes.TEXT,
      allowNull: true,
      defaultValue: null,
    },
    linkedin: {
      type: DataTypes.TEXT,
      allowNull: true,
      defaultValue: null,
    },
    slug: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    view: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    status: {
      type: DataTypes.ENUM('active', 'archive'),
      allowNull: true,
      defaultValue: 'active',
    },
  });

  Careers.associate = function (models) {
    Careers.belongsTo(models.Department, {
      foreignKey: 'departmentId',
    });
  };

  return Careers;
};
