'use strict';

/**
 * Skill Schema
 */

module.exports = function (sequelize, DataTypes) {
  const Skill = sequelize.define('Skill', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a name',
        },
      },
    },
    type: {
      type: DataTypes.ENUM('main', 'additional', 'hybrid'),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a type',
        },
        isIn: [['main', 'additional', 'hybrid']],
      },
    },
  });

  return Skill;
};
