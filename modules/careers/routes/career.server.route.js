'use strict';

/**
 * Module dependencies
 */

const path = require('path');
const careerController = require('../controller/career.server.controller');
const policy = require(path.resolve('./config/lib/acl_auth'));

/**
 * Routes
 */
module.exports = function (app) {
  //route for cms
  app
    .route('/api/career')
    .all(policy.isAllowed)
    .get(careerController.list)
    .post(careerController.create);

  app
    .route('/api/career/:id')
    .all(policy.isAllowed)
    .get(careerController.read)
    .put(careerController.update)
    .delete(careerController.delete);

  app
    .route('/api/career-skill')
    .get(careerController.getSkiils)
    .post(careerController.addSkill);

  app.route('/api/public/career').get(careerController.publicList);
  app.route('/api/public/career/:slug').get(careerController.publicRead);
  app
    .route('/api/public/career-department/:name')
    .get(careerController.publicListByDepartment);
};
