'use strict';

var validator = require('validator');

/**
 * Render the main application page
 */
exports.renderIndex = function (req, res) {
  res.send('');
};

/**
 * Render the server error page
 */
exports.renderServerError = function (req, res) {
  res.status(500).send('500 Error');
};

/**
 * Render the server not found responses
 * Performs content-negotiation on the Accept HTTP header
 */
exports.renderNotFound = function (req, res) {
  res.status(404).format({
    'application/json': function () {
      res.json({
        error: 'Path not found',
      });
    },
    default: function () {
      res.send('Path not found');
    },
  });
};
