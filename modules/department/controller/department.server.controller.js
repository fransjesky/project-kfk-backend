'use strict';

/**
 * Module dependencies
 */

const path = require('path');
const db = require(path.resolve('./config/lib/sequelize'));
const Department = db.Department;
const errorHandler = require(path.resolve(
  './modules/core/controllers/errors.server.controller'
));
const { Op } = require('sequelize');

/**
 * list of career
 */
exports.listDropDown = async function (req, res) {
  try {
    const departments = await Department.findAll({
      attributes: ['id', 'name'],
    });
    return res.status(200).json({
      data: departments,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};
