'use strict';

/**
 * Department Schema
 */

module.exports = function (sequelize, DataTypes) {
  const Department = sequelize.define('Department', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a name',
        },
      },
    },
    color: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a color',
        },
      },
    },
    logo: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a logo',
        },
      },
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a description',
        },
      },
    },
  });

  Department.associate = function (models) {
    Department.hasMany(models.Member, {
      foreignKey: 'departmentId',
    });
    Department.hasMany(models.Career, {
      foreignKey: 'departmentId',
    });
  };

  return Department;
};
