'use strict';

/**
 * Module dependencies
 */

const path = require('path');
const departmentController = require('../controller/department.server.controller');
const policy = require(path.resolve('./config/lib/acl_auth'));

/**
 * Routes
 */
module.exports = function (app) {
  //route for cms
  app.route('/api/department/drop-down').get(departmentController.listDropDown);
  app
    .route('/api/public/department/drop-down')
    .get(departmentController.listDropDown);
};
