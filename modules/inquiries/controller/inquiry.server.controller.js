'use strict';

/**
 * Module dependencies
 */
const path = require('path');
const errorHandler = require(path.resolve(
  './modules/core/controllers/errors.server.controller'
));
const xcMailer = require(path.resolve('./config/lib/send_email'));
const config = require(path.resolve('./config/config'));
const fs = require('fs');
const db = require(path.resolve('./config/lib/sequelize'));
const { Op } = require('sequelize');
const Setting = db.Setting;
const Inquiry = db.Inquiry;

/**
 * list of inquiry
 */
exports.list = async function (req, res) {
  try {
    const {
      keyword,
      sort = 'createdAt',
      order = 'DESC',
      limit = 5,
      offset = 0,
    } = req.query;

    let query = {
      where: {},
      order: [[sort, order]],
      limit,
      offset,
    };

    if (keyword) {
      query.where[Op.or] = [
        {
          name: { [Op.iLike]: `%${keyword}%` },
        },
        {
          subject: { [Op.iLike]: `%${keyword}%` },
        },
        {
          email: { [Op.iLike]: `%${keyword}%` },
        },
      ];
    }

    const inquiries = await Inquiry.findAndCountAll(query);
    return res.status(200).json({
      count: inquiries.count,
      data: inquiries.rows,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

exports.read = async function (req, res) {
  const id = req.params.id;

  try {
    const inquiry = await Inquiry.findOne({ where: { id } });

    if (!inquiry) {
      return res.status(400).send({
        message: `Inquiry with id ${id} is not found`,
      });
    }

    return res.status(200).json({
      data: inquiry,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * Send email for inquiries
 */
exports.sendInquiries = async function (req, res) {
  try {
    const { email, subject, message, name } = req.body;

    const setting = await Setting.findAll({
      order: [['id', 'ASC']],
      attributes: ['id', 'optionName', 'optionValue'],
    });

    const templates = 'modules/inquiries/templates/thank-you.template.html';

    const mailOptionForInternal = {
      to: setting[1].optionValue,
      from: email,
      subject: subject,
      text: `Hello Xcidic, I am ${name}. ${message}`,
    };

    const mailOptionForUser = {
      to: email,
      from: setting[2].optionValue,
      subject: 'Thank You!',
      html: fs.readFileSync(templates, 'utf-8'),
    };

    await xcMailer.sendEmail(mailOptionForInternal);

    await xcMailer.sendEmail(mailOptionForUser);

    const inquiry = await Inquiry.create({
      name,
      email,
      subject,
      message,
    });

    return res.status(200).send({
      message: 'Thank you We will get back to you in 2-3 business days.',
      data: inquiry,
    });
  } catch (e) {
    console.log(e);
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * get status form
 */
exports.getStatus = async function (req, res) {
  try {
    const setting = await Setting.findOne({
      where: { optionName: 'isEnableEnquiries' },
    });

    const status = setting.optionValue === 'true';

    return res.status(200).send({
      data: {
        status,
      },
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};
