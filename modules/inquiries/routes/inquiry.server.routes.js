'use strict';

/**
 * Module dependencies
 */
const inquiryController = require('../controller/inquiry.server.controller');

/**
 * Routes
 */
module.exports = function (app) {
  app.route('/api/inquiry').get(inquiryController.list);
  app.route('/api/inquiry/:id').get(inquiryController.read);

  app
    .route('/api/public/inquiry')
    .post(inquiryController.sendInquiries)
    .get(inquiryController.getStatus);
};
