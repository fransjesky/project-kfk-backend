'use strict';

/**
 * Module dependencies
 */

const path = require('path');
const db = require(path.resolve('./config/lib/sequelize'));
const Journey = db.Journey;
const errorHandler = require(path.resolve(
  './modules/core/controllers/errors.server.controller'
));
const { Op } = require('sequelize');

/**
 * list of journey
 */
exports.list = async function (req, res) {
  const {
    keyword,
    sort = 'createdAt',
    order = 'DESC',
    limit = 5,
    offset = 0,
  } = req.query;

  let query = {
    where: {
      [Op.and]: [],
    },
    order: [[sort, order]],
    limit,
    offset,
  };

  if (keyword) {
    query.where[Op.and].push({ year: { [Op.iLike]: `%${keyword}%` } });
  }

  try {
    const journeys = await Journey.findAndCountAll(query);
    return res.status(200).json({ count: journeys.count, data: journeys.rows });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * list of journey for main website
 */
exports.publicList = async function (req, res) {
  try {
    const journeys = await Journey.findAll({
      attributes: [
        'id',
        'year',
        'text',
        'mainColor',
        'secondaryColor',
        'dimension',
      ],
      order: [
        ['createdAt', 'ASC'],
        ['year', 'ASC'],
      ],
    });

    return res.status(200).json({
      data: journeys,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * create journey
 */
exports.create = async function (req, res) {
  const payload = req.body;

  try {
    const journey = await Journey.create(payload);
    return res.json({
      message: 'Journey has been Added successfully',
      data: journey,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * show one journey
 */
exports.read = async function (req, res) {
  const id = req.params.id;

  try {
    const journey = await Journey.findOne({ where: { id } });

    //check if journey not with id params not found
    if (!journey) {
      return res.status(400).send({
        message: `Journey with id ${id} is not found`,
      });
    }

    return res.status(200).json({
      data: journey,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * update journey
 */
exports.update = async function (req, res) {
  const id = req.params.id;
  const payload = req.body;

  try {
    const journey = await Journey.findOne({ where: { id } });

    //check if journey not with id params not found
    if (!journey) {
      return res.status(400).send({
        message: `Journey with id ${id} is not found`,
      });
    }

    await journey.update(payload);

    return res.status(200).json({
      message: 'Journey has been edited successfully',
      data: journey,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * delete journey
 */
exports.delete = async function (req, res) {
  const id = req.params.id;

  try {
    const journey = await Journey.findOne({ where: { id } });

    //check if journey not with id params not found
    if (!journey) {
      return res.status(400).send({
        message: `Journey with id ${id} is not found`,
      });
    }

    await journey.destroy();

    return res.status(200).json({
      message: 'Journey has been deleted successfully',
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};
