'use strict';

/**
 * Journey Schema
 */

module.exports = function (sequelize, DataTypes) {
  const Journey = sequelize.define('Journey', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    year: {
      type: DataTypes.STRING(4),
      unique: true,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a year',
        },
      },
    },
    text: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a text',
        },
      },
    },
    mainColor: {
      type: DataTypes.STRING(7),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a main color',
        },
      },
    },
    secondaryColor: {
      type: DataTypes.STRING(7),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a secondary color',
        },
      },
    },
    dimension: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a dimension',
        },
      },
    },
  });

  return Journey;
};
