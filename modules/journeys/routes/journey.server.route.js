'use strict';

/**
 * Module dependencies
 */
const path = require('path');
const journeyController = require('../controller/journey.server.controller');
const policy = require(path.resolve('./config/lib/acl_auth'));

/**
 * Routes
 */
module.exports = function (app) {
  //route for cms
  app
    .route('/api/journey')
    .all(policy.isAllowed)
    .get(journeyController.list)
    .post(journeyController.create);

  app
    .route('/api/journey/:id')
    .all(policy.isAllowed)
    .get(journeyController.read)
    .put(journeyController.update)
    .delete(journeyController.delete);

  app.route('/api/public/journey').get(journeyController.publicList);
};
