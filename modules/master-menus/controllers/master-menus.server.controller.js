'use strict';

/**
 * Module dependencies
 */
const path = require('path'),
  db = require(path.resolve('./config/lib/sequelize')),
  errorHandler = require(path.resolve(
    './modules/core/controllers/errors.server.controller'
  )),
  { Op } = require('sequelize');

/**
 * Create an masterMenu
 */
exports.create = async function (req, res) {
  let query = 'SELECT MAX("_id") FROM "public"."MasterMenus"';
  let response = await db.sequelize.query(query, {
    type: db.sequelize.QueryTypes.SELECT,
  });
  let lastId = response[0].max;
  if (!req.body.parentMenu || req.body.parentMenu._id === 'None') {
    delete req.body.parentMenu;
  }
  const masterMenu = req.body;
  masterMenu._id = lastId + 1;
  masterMenu.parentId = masterMenu.parentMenu;
  masterMenu.createdBy = req.user._id;
  if (req.body.link && req.body.link.length > 0) {
    // parent menu
    masterMenu.abstract = false;
    masterMenu.type = '';
  } else {
    masterMenu.abstract = true;
    masterMenu.type = 'dropdown';
  }
  try {
    const masterMenuResult = await db.MasterMenu.create(masterMenu);
    res.json({
      message: 'Master menu has been created',
      data: masterMenuResult,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * Show the current masterMenu
 */
exports.read = async function (req, res) {
  let _id = req.params.masterMenuId;
  try {
    const masterMenu = await db.MasterMenu.findOne({
      where: {
        _id: _id,
      },
      include: [
        { model: db.User, as: 'User', attributes: ['_id', 'displayName'] },
        { model: db.MasterMenu, as: 'parentMenu' },
      ],
    });
    if (!masterMenu) {
      return res.status(400).send({
        message: 'Failed to load master menu id ' + _id,
      });
    }
    res.json(masterMenu);
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * Update an masterMenu
 */
exports.update = async function (req, res) {
  let _id = req.params.masterMenuId;
  let masterMenu = req.body;
  if (masterMenu.parentMenu === '') {
    masterMenu.parentId = null;
  } else {
    masterMenu.parentId = masterMenu.parentMenu;
  }
  try {
    await db.MasterMenu.update(masterMenu, { where: { _id: _id } });
    res.json({
      message: 'Master menu has been updated',
      data: masterMenu,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * Delete an masterMenu
 */
exports.delete = async function (req, res) {
  let _id = req.params.masterMenuId;
  try {
    const masterMenu = await db.MasterMenu.destroy({
      where: {
        _id: _id,
      },
    });
    res.json({
      message: 'Master menu has been deleted',
      data: masterMenu,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * Bulk Delete an masterMenu
 */
exports.bulkDelete = async function (req, res) {
  let ids = req.body.ids;
  if (ids.length > 0) {
    try {
      let masterMenuDeleted = await db.MasterMenu.destroy({
        where: { _id: { [Op.in]: ids } },
      });
      res.json({
        message: masterMenuDeleted + ' acl has been deleted',
        data: masterMenuDeleted,
      });
    } catch (e) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(e),
      });
    }
  }
};

/**
 * List of MasterMenus
 */
exports.list = async function (req, res) {
  var searchCriteria = {};
  let joinCreatedBy = {
    association: 'User',
    attributes: ['_id', 'displayName'],
  };
  let joinParentMenu = { model: db.MasterMenu, as: 'parentMenu' };
  if (req.query.search || req.query.advanceSearch) {
    if (req.query.advanceSearch) {
      searchCriteria = { [Op.and]: [] };
      if (req.query.title) {
        searchCriteria[Op.and].push({
          title: { [Op.iLike]: '%' + req.query.title + '%' },
        });
      }
      if (req.query.icon) {
        searchCriteria[Op.and].push({
          icon: { [Op.iLike]: '%' + req.query.icon + '%' },
        });
      }
      if (req.query.link) {
        searchCriteria[Op.and].push({
          link: { [Op.iLike]: '%' + req.query.link + '%' },
        });
      }
      if (req.query.parentMenu !== 'None' && req.query.parentMenu) {
        searchCriteria[Op.and].push({
          parentId: req.query.parentMenu,
        });
      }
      if (req.query.order) {
        searchCriteria[Op.and].push({
          order: req.query.order,
        });
      }
      if (req.query.displayName) {
        joinCreatedBy.where = {
          displayName: { [Op.iLike]: '%' + req.query.displayName + '%' },
        };
      }
      if (req.query.parentName) {
        joinParentMenu.where = {
          title: { [Op.iLike]: '%' + req.query.parentName + '%' },
        };
      }
      if (searchCriteria[Op.and] && searchCriteria[Op.and].length === 0) {
        searchCriteria = {};
      }
    }
  }
  try {
    let orderBy =
      req.query.order && req.query.orderMethod
        ? [req.query.order, req.query.orderMethod]
        : ['createdAt', 'ASC'];
    const masterMenus = await db.MasterMenu.findAndCountAll({
      where: searchCriteria,
      include: [joinCreatedBy, joinParentMenu],
      order: [orderBy],
      offset: req.query.skip || 0,
      limit: req.query.limit || 100,
    });
    res.json({ count: masterMenus.count, data: masterMenus.rows });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

exports.countClick = async function (req, res) {
  let _id = req.params.masterMenuId;
  let masterMenu = req.body;
  try {
    const masterMenuResult = await db.MasterMenu.update(
      { countClick: db.sequelize.literal('"countClick" + 1') },
      { where: { _id: _id } }
    );
    res.json(masterMenuResult);
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};
