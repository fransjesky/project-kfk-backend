'use strict';

module.exports = function (sequelize, DataTypes) {
  const MasterMenu = sequelize.define('MasterMenu', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    title: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true,
      },
    },
    icon: {
      type: DataTypes.STRING,
      defaultValue: ' ',
    },
    path: {
      type: DataTypes.STRING,
      defaultValue: ' ',
    },
    type: {
      type: DataTypes.STRING,
      defaultValue: ' ',
    },
    abstract: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    order: {
      type: DataTypes.INTEGER,
      validate: {
        notEmpty: true,
      },
    },
  });
  MasterMenu.associate = function (models) {
    MasterMenu.belongsTo(models.MasterMenu, {
      as: 'parentMenu',
      foreignKey: 'parentId',
      useJunctionTable: false,
    });
    MasterMenu.belongsToMany(models.Role, {
      through: 'RoleMenu',
      foreignKey: 'menuId',
    });
    MasterMenu.hasMany(models.Acl);
  };

  return MasterMenu;
};
