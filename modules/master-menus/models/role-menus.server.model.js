'use strict';

module.exports = function (sequelize, DataTypes) {
  var RoleMenu = sequelize.define('RoleMenu', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
  });

  return RoleMenu;
};
