'use strict';

/**
 * Module dependencies
 */
const path = require('path');
const policy = require(path.resolve('./config/lib/acl_auth'));
const masterMenus = require('../controllers/master-menus.server.controller');

module.exports = function (app) {
  // MasterMenus collection routes
  app
    .route('/api/master-menus')
    .all(policy.isAllowed)
    .get(masterMenus.list)
    .post(masterMenus.create);

  // Bulk Delete masterMenu routes
  app
    .route('/api/master-menus/bulk-delete')
    .all(policy.isAllowed)
    .post(masterMenus.bulkDelete);
  // Single masterMenu routes
  app
    .route('/api/master-menus/:masterMenuId')
    .all(policy.isAllowed)
    .get(masterMenus.read)
    .put(masterMenus.update)
    .delete(masterMenus.delete);
  app
    .route('/api/master-menus/:masterMenuId/count')
    .all(policy.isAllowed)
    .put(masterMenus.countClick);
};
