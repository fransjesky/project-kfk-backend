'use strict';

/**
 * Module dependencies
 */

const path = require('path');
const db = require(path.resolve('./config/lib/sequelize'));
const Member = db.Member;
const errorHandler = require(path.resolve(
  './modules/core/controllers/errors.server.controller'
));
const { Op } = require('sequelize');

/**
 * list of member
 */
exports.list = async function (req, res) {
  try {
    const { keyword, department, limit = 5, offset = 0 } = req.query;

    let query = {
      where: {
        [Op.and]: [],
      },
      order: [
        ['departmentId', 'asc'],
        ['order', 'asc'],
      ],
      limit,
      offset,
      attributes: {
        exclude: ['updatedAt', 'departmentId'],
      },
      include: {
        model: db.Department,
        attributes: ['id', 'name', 'color'],
        where: {
          [Op.and]: [],
        },
      },
    };

    if (keyword) {
      query.where[Op.and].push({ name: { [Op.iLike]: `%${keyword}%` } });
    }

    if (department) {
      query.include.where[Op.and].push({
        name: { [Op.iLike]: `%${department}%` },
      });
    }

    const members = await Member.findAndCountAll(query);
    return res.status(200).json({
      count: members.count,
      data: members.rows,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * list of member in main website
 */
exports.publicList = async function (req, res) {
  const { department } = req.query;

  let query = {
    order: [['order', 'asc']],
    attributes: {
      exclude: ['updatedAt', 'departmentId'],
    },
    include: {
      model: db.Department,
      attributes: {
        exclude: ['createdAt', 'updatedAt'],
      },
      where: {
        [Op.and]: [],
      },
    },
  };

  if (department) {
    query.include.where[Op.and].push({
      name: { [Op.iLike]: `%${department}%` },
    });
  }

  try {
    const members = await Member.findAll(query);

    return res.status(200).json({
      data: members,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * create member
 */
exports.create = async function (req, res) {
  const payload = req.body;
  try {
    const member = await Member.create(payload);
    return res.json({
      message: 'Member has been added',
      data: member,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * show one member
 */
exports.read = async function (req, res) {
  const id = req.params.id;

  try {
    const member = await Member.findOne({
      where: { id },
      attributes: {
        exclude: ['updatedAt', 'departmentId'],
      },
      include: {
        model: db.Department,
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      },
    });

    //check if member not with id params not found
    if (!member) {
      return res.status(400).send({
        message: `Member with id ${id} is not found`,
      });
    }

    return res.status(200).json({
      data: member,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * update member
 */
exports.update = async function (req, res) {
  const id = req.params.id;
  const { name, role, departmentId, text, avatar, order } = req.body;

  try {
    //query
    const member = await Member.findOne({ where: { id } });

    //check if member not with id params not found
    if (!member) {
      return res.status(400).send({
        message: `member with id ${id} is not found`,
      });
    }

    const memberWithMaxOrder = await Member.findOne({
      order: [['order', 'desc']],
      attributes: ['order'],
      where: {
        departmentId:
          departmentId === member.departmentId
            ? departmentId
            : member.departmentId,
      },
    });

    const maxOrder = memberWithMaxOrder ? memberWithMaxOrder.order : 1;

    if (order != member.order) {
      //check if departemnt dont change
      if (departmentId == member.departmentId) {
        const maxBetween = order > member.order ? member.order : maxOrder;
        // //get all tech stack by category
        const members = await Member.findAll({
          attributes: ['id', 'order'],
          where: {
            departmentId: member.departmentId,
            order: {
              [Op.between]:
                order < maxBetween ? [order, maxBetween] : [maxBetween, order],
              [Op.notIn]: [member.order],
            },
          },
        });

        //loop for update each item in order
        members.forEach(async (item) => {
          //chek if order update with old order
          if (order > member.order) {
            await item.update({
              order: item.order <= order ? item.order - 1 : item.order,
            });
          } else {
            await item.update({
              order: item.order <= member.order ? item.order + 1 : item.order,
            });
          }
        });
      } else {
        const maxBetween = order > member.order ? member.order : maxOrder;

        const members = await Member.findAll({
          attributes: ['id', 'order'],
          where: {
            departmentId: member.departmentId,
            order: {
              [Op.between]:
                order < maxBetween ? [order, maxBetween] : [maxBetween, order],
              [Op.notIn]: [member.order],
            },
          },
        });

        members.forEach((item) => {
          if (item.order > member.order) {
            item.update({
              order: item.order - 1,
            });
          }
        });
      }
    }

    await member.update({
      name,
      role,
      departmentId,
      text,
      avatar,
      order,
    });

    return res.status(200).json({
      message: 'member successfully edited',
      data: member,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * delete member
 */
exports.delete = async function (req, res) {
  const id = req.params.id;

  try {
    const member = await Member.findOne({ where: { id } });

    //check if member not with id params not found
    if (!member) {
      return res.status(400).send({
        message: `Member with id ${id} is not found`,
      });
    }

    const memberWithMaxOrder = await Member.findOne({
      order: [['order', 'desc']],
      attributes: ['order'],
      where: {
        departmentId: member.departmentId,
      },
    });

    await Member.findAll({
      attributes: ['id', 'order'],
      where: {
        departmentId: member.departmentId,
        order: {
          [Op.between]: [member.order, memberWithMaxOrder.order],
        },
      },
    }).then(function (result) {
      if (result) {
        result.forEach((item) => {
          item.update({
            order: item.order - 1,
          });
        });
      }
    });

    await member.destroy();

    return res.status(200).json({
      message: 'Member have been deleted',
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

exports.showOrder = async function (req, res) {
  try {
    const { departmentId } = req.query;

    if (!departmentId) {
      return res.status(400).send({
        message: 'departmentId is required',
      });
    }

    const member = await Member.findOne({
      order: [['order', 'desc']],
      attributes: ['order'],
      where: {
        departmentId,
      },
    });

    const lastOrderInMember = member ? member.order + 1 : 1;

    let data = [];

    for (let i = 1; i <= lastOrderInMember; i++) {
      data.push(i);
    }

    return res.status(200).json({
      data,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};
