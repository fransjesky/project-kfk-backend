'use strict';

/**
 * Member Schema
 */

module.exports = function (sequelize, DataTypes) {
  const Member = sequelize.define('Member', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a name',
        },
      },
    },
    role: {
      type: DataTypes.STRING(255),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a role',
        },
      },
    },
    text: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a text',
        },
      },
    },
    avatar: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a order',
        },
      },
    },
    order: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a order',
        },
        isInt: {
          msg: 'Must be an integer number',
        },
      },
    },
  });

  Member.associate = function (models) {
    Member.belongsTo(models.Department, {
      foreignKey: 'departmentId',
    });
  };

  return Member;
};
