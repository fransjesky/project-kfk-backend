'use strict';

/**
 * Module dependencies
 */

const path = require('path');
const memberController = require('../controller/member.server.controller');
const policy = require(path.resolve('./config/lib/acl_auth'));

/**
 * Routes
 */
module.exports = function (app) {
  //route for cms
  app
    .route('/api/member')
    .all(policy.isAllowed)
    .get(memberController.list)
    .post(memberController.create);

  app.route('/api/member/number').get(memberController.showOrder);

  app
    .route('/api/member/:id')
    .all(policy.isAllowed)
    .get(memberController.read)
    .put(memberController.update)
    .delete(memberController.delete);

  app.route('/api/public/member').get(memberController.publicList);
};
