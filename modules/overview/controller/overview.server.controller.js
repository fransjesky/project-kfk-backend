'use strict';

/**
 * Module dependencies
 */

const path = require('path');
const db = require(path.resolve('./config/lib/sequelize'));
const errorHandler = require(path.resolve(
  './modules/core/controllers/errors.server.controller'
));
const sequelize = require('sequelize');
const moment = require('moment');
const { Op } = require('sequelize');
const { add } = require('lodash');

exports.portfolio = async function (req, res) {
  try {
    const query = {
      limit: 2,
      order: [['createdAt', 'DESC']],
      attributes: [
        'id',
        'name',
        'description',
        'thumbnail',
        'detailLink',
        'status',
      ],
    };

    const portfolios = await db.Portfolio.findAll(query);
    return res.status(200).json({ data: portfolios });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

exports.activeJobs = async function (req, res) {
  try {
    const query = {
      limit: 4,
      order: [['createdAt', 'DESC']],
      where: {
        status: 'active',
      },
      attributes: ['id', 'name', 'jobType', 'view', 'status', 'createdAt'],
      include: {
        model: db.Department,
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      },
    };

    const jobs = await db.Career.findAll(query);
    return res.status(200).json({ data: jobs });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

exports.inquiry = async function (req, res) {
  try {
    const {
      sort = 'createdAt',
      order = 'DESC',
      limit = 5,
      offset = 0,
    } = req.query;

    let query = {
      order: [[sort, order]],
      limit,
      offset,
      attributes: ['id', 'name', 'createdAt'],
    };

    const inquiries = await db.Inquiry.findAll(query);
    return res.status(200).json({
      data: inquiries,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

exports.countVacancy = async function (req, res) {
  try {
    const activeVacancy = await db.Career.count({
      where: { status: 'active' },
    });

    const allVacancy = await db.Career.count();

    return res
      .status(200)
      .json({ data: { activeJob: activeVacancy, allJob: allVacancy } });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

exports.countTeam = async function (req, res) {
  try {
    const teams = await db.Member.count();

    return res.status(200).json({
      data: {
        team: teams,
        department: 4,
      },
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

exports.countUsers = async function (req, res) {
  try {
    const visitorToday = await db.Visitor.findAll({
      attributes: ['ip', [sequelize.fn('COUNT', 'ip'), 'total']],
      group: ['ip'],
      where: {
        createdAt: {
          [Op.gt]: moment().format('YYYY-MM-DD 00:00'),
          [Op.lte]: moment().format('YYYY-MM-DD 23:59'),
        },
      },
    });

    const visitorYesterday = await db.Visitor.findAll({
      attributes: ['ip', [sequelize.fn('COUNT', 'ip'), 'total']],
      group: ['ip'],
      where: {
        createdAt: {
          [Op.between]: [
            moment().subtract(1, 'days').startOf('day'),
            moment().startOf('day'),
          ],
        },
      },
    });

    const countVisitorToday = visitorToday.length;
    const countVisitorYesterday = visitorYesterday.length;

    let persentace;
    if (countVisitorToday > countVisitorYesterday) {
      persentace =
        ((countVisitorToday - countVisitorYesterday) / countVisitorToday) * 100;
      persentace = Math.round(persentace * 10) / 10;
      persentace = `${persentace}%`;
    } else {
      persentace =
        ((countVisitorYesterday - countVisitorToday) / countVisitorYesterday) *
        100;
      persentace = Math.round(persentace * 10) / 10;
      persentace = `-${persentace}%`;
    }

    return res.status(200).json({
      data: {
        todayVisitor: countVisitorToday,
        persentace: persentace,
      },
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};
