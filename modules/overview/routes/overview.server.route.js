'use strict';

/**
 * Module dependencies
 */

const overviewController = require('../controller/overview.server.controller');

/**
 * Routes
 */
module.exports = function (app) {
  //route for cms
  app.route('/api/overview/vacancy').get(overviewController.countVacancy);
  app.route('/api/overview/team').get(overviewController.countTeam);
  app.route('/api/overview/portfolio').get(overviewController.portfolio);
  app.route('/api/overview/jobs').get(overviewController.activeJobs);
  app.route('/api/overview/inquiry').get(overviewController.inquiry);
  app.route('/api/overview/user').get(overviewController.countUsers);
};
