'use strict';

/**
 * Module dependencies
 */

const path = require('path');
const db = require(path.resolve('./config/lib/sequelize'));
const Patner = db.Patner;
const errorHandler = require(path.resolve(
  './modules/core/controllers/errors.server.controller'
));
const { Op } = require('sequelize');

/**
 * list of patner
 */
exports.list = async function (req, res) {
  try {
    const {
      keyword,
      sort = 'order',
      order = 'ASC',
      limit = 5,
      offset = 0,
    } = req.query;

    let query = {
      where: {
        [Op.and]: [],
      },
      order: [[sort, order]],
      limit,
      offset,
    };

    if (keyword) {
      query.where[Op.and].push({ name: { [Op.iLike]: `%${keyword}%` } });
    }

    const patners = await Patner.findAndCountAll(query);
    return res.status(200).json({
      count: patners.count,
      data: patners.rows,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * list of patner for main website
 */
exports.publicList = async function (req, res) {
  try {
    const patners = await Patner.findAll({
      attributes: ['id', 'logo', 'order'],
      order: [['order', 'asc']],
    });
    return res.status(200).json({
      data: patners,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * create patner
 */
exports.create = async function (req, res) {
  const payload = req.body;
  try {
    const patner = await Patner.create(payload);
    return res.json({
      message: 'a new patner has been added successfully',
      data: patner,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * show one patner
 */
exports.read = async function (req, res) {
  const id = req.params.id;

  try {
    const patner = await Patner.findOne({ where: { id } });

    //check if patner not with id params not found
    if (!patner) {
      return res.status(400).send({
        message: `Patner with id ${id} is not found`,
      });
    }

    return res.status(200).json({
      data: patner,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * update patner
 */
exports.update = async function (req, res) {
  const id = req.params.id;
  const { name, logo, order } = req.body;

  try {
    //query
    const patner = await Patner.findOne({ where: { id } });

    const patnerWithMaxOrder = await Patner.findOne({
      order: [['order', 'desc']],
      attributes: ['order'],
    });

    //check if patner not with id params not found
    if (!patner) {
      return res.status(400).send({
        message: `patner with id ${id} is not found`,
      });
    }

    if (order != patner.order) {
      const maxBetween =
        order > patner.order ? patner.order : patnerWithMaxOrder.order;

      const patners = await Patner.findAll({
        attributes: ['id', 'order'],
        where: {
          order: {
            [Op.between]:
              order < maxBetween ? [order, maxBetween] : [maxBetween, order],
            [Op.notIn]: [patner.order],
          },
        },
      });

      patners.forEach(async (item) => {
        if (order > patner.order) {
          await item.update({
            order: item.order <= order ? item.order - 1 : item.order,
          });
        } else {
          await item.update({
            order: item.order <= patner.order ? item.order + 1 : item.order,
          });
        }
      });
    }

    await patner.update({
      name,
      logo,
      order,
    });

    return res.status(200).json({
      message: 'patner successfully edited',
      data: patner,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * delete patner
 */
exports.delete = async function (req, res) {
  const id = req.params.id;

  try {
    const patner = await Patner.findOne({ where: { id } });
    const patnerWithMaxOrder = await Patner.findOne({
      order: [['order', 'desc']],
      attributes: ['order'],
    });

    //check if Patner not with id params not found
    if (!patner) {
      return res.status(400).send({
        message: `patner with id ${id} is not found`,
      });
    }

    await Patner.findAll({
      attributes: ['id', 'order'],
      where: {
        order: {
          [Op.between]: [patner.order, patnerWithMaxOrder.order],
        },
      },
    }).then(function (result) {
      if (result) {
        result.forEach((item) => {
          item.update({
            order: item.order - 1,
          });
        });
      }
    });

    await patner.destroy();

    return res.status(200).json({
      message: `${patner.name} has been deleted successfully`,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

exports.showOrder = async function (req, res) {
  try {
    const patner = await Patner.findOne({
      order: [['order', 'desc']],
      attributes: ['order'],
    });

    const lastOrderInPatner = patner ? patner.order + 1 : 1;

    let data = [];

    for (let i = 1; i <= lastOrderInPatner; i++) {
      data.push(i);
    }

    return res.status(200).json({
      data,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};
