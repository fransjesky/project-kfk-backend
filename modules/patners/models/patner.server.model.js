'use strict';

/**
 * Patner Schema
 */

module.exports = function (sequelize, DataTypes) {
  const Patner = sequelize.define('Patner', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a name',
        },
      },
    },
    logo: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a logo',
        },
      },
    },
    order: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a order',
        },
        isInt: {
          msg: 'Must be an integer number',
        },
      },
    },
  });

  return Patner;
};
