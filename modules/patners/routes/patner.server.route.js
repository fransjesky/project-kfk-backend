'use strict';

/**
 * Module dependencies
 */
const path = require('path');
const patnerController = require('../controller/patner.server.controller');
const policy = require(path.resolve('./config/lib/acl_auth'));

/**
 * Routes
 */
module.exports = function (app) {
  //route for cms
  app
    .route('/api/patner')
    .all(policy.isAllowed)
    .get(patnerController.list)
    .post(patnerController.create);

  app.route('/api/patner/number').get(patnerController.showOrder);

  app
    .route('/api/patner/:id')
    .all(policy.isAllowed)
    .get(patnerController.read)
    .put(patnerController.update)
    .delete(patnerController.delete);

  app.route('/api/public/patner').get(patnerController.publicList);
};
