'use strict';

/**
 * module dependecies
 */
const path = require('path');
const db = require(path.resolve('./config/lib/sequelize'));
const Portfolio = db.Portfolio;
const errorHandler = require(path.resolve(
  './modules/core/controllers/errors.server.controller'
));
const { Op } = require('sequelize');

/**
 * list of portfolio
 */
exports.list = async function (req, res) {
  const {
    keyword,
    sort = 'createdAt',
    order = 'DESC',
    limit = 5,
    offset = 0,
  } = req.query;

  let query = {
    order: [[sort, order]],
    limit,
    offset,
    attributes: [
      'id',
      'name',
      'projectCompany',
      'detailLink',
      'thumbnail',
      'status',
    ],
  };

  if (keyword) {
    query.where = {
      [Op.and]: {
        name: { [Op.iLike]: `%${keyword}%` },
      },
    };
  }

  try {
    const portfolio = await Portfolio.findAndCountAll(query);
    return res
      .status(200)
      .json({ count: portfolio.count, data: portfolio.rows });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * list of portfolio fro main website
 */
exports.publicList = async function (req, res) {
  try {
    const {
      sort = 'createdAt',
      order = 'DESC',
      limit = 5,
      offset = 0,
    } = req.query;

    let query = {
      order: [[sort, order]],
      limit,
      offset,
      where: {
        status: 'active',
      },
      attributes: [
        'id',
        'name',
        'projectType',
        'description',
        'detailLink',
        'thumbnail',
      ],
    };
    const portfolios = await Portfolio.findAll(query);

    return res.status(200).json({ data: portfolios });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * create portfolio
 */
exports.create = async function (req, res) {
  const payload = req.body;

  try {
    const portfolio = await Portfolio.create(payload);
    return res.json({
      message: 'A new portofolio has been Added successfully',
      data: portfolio,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * read porfolio
 */
exports.read = async function (req, res) {
  const id = req.params.id;

  try {
    const portfolio = await Portfolio.findOne({ where: { id } });

    if (!portfolio) {
      return res.status(400).send({
        message: `Portfolio with id ${id} is not found`,
      });
    }

    return res.status(200).json({
      data: portfolio,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * update porfolio
 */
exports.update = async function (req, res) {
  const id = req.params.id;
  const payload = req.body;

  try {
    const portfolio = await Portfolio.findOne({ where: { id } });

    if (!portfolio) {
      return res.status(400).send({
        message: `Portfolio with id ${id} is not found`,
      });
    }

    await portfolio.update(payload);

    if (payload.status) {
      if (payload.status == 'archive') {
        return res.status(200).json({
          message: `${portfolio.name} has been Achieved successfully`,
          data: portfolio,
        });
      }

      if (payload.status == 'active') {
        return res.status(200).json({
          message: `${portfolio.name} has been Showed successfully`,
          data: portfolio,
        });
      }
    }

    return res.status(200).json({
      message: `${portfolio.name} has been edited successfully`,
      data: portfolio,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * delete portfolio
 */
exports.delete = async function (req, res) {
  const id = req.params.id;

  try {
    const portfolio = await Portfolio.findOne({ where: { id } });

    if (!portfolio) {
      return res.status(400).send({
        message: `Portfolio with id ${id} is not found`,
      });
    }

    await portfolio.destroy();

    return res.status(200).json({
      message: `${portfolio.name} has been deleted successfully`,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};
