'use strict';

/**
 * Portfolio Schema
 */

module.exports = function (sequelize, DataTypes) {
  const Portfolio = sequelize.define('Portfolio', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a field',
        },
      },
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a field',
        },
      },
    },
    projectCompany: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input project company',
        },
      },
    },
    projectType: {
      type: DataTypes.ARRAY(DataTypes.TEXT),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input project type',
        },
      },
    },
    detailLink: {
      type: DataTypes.STRING(255),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a field',
        },
      },
    },
    thumbnail: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a field',
        },
      },
    },
    status: {
      type: DataTypes.ENUM('active', 'archive'),
      allowNull: true,
      defaultValue: 'active',
    },
  });

  return Portfolio;
};
