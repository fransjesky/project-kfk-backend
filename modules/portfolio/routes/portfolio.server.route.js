'use strict';

/**
 * Module dependencies
 */

const path = require('path');
const portfolioController = require('../controller/portfolio.server.controller');
const policy = require(path.resolve('./config/lib/acl_auth'));

/**
 * Routes
 */
module.exports = function (app) {
  //route for cms
  app
    .route('/api/portfolio')
    .all(policy.isAllowed)
    .get(portfolioController.list)
    .post(portfolioController.create);

  app
    .route('/api/portfolio/:id')
    .all(policy.isAllowed)
    .get(portfolioController.read)
    .put(portfolioController.update)
    .delete(portfolioController.delete);

  app.route('/api/public/portfolio').get(portfolioController.publicList);
};
