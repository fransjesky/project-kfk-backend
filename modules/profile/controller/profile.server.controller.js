'use strict';

/**
 * module dependency
 */
const path = require('path');
const db = require(path.resolve('./config/lib/sequelize'));
const Profile = db.Profile;
const errorHandler = require(path.resolve(
  './modules/core/controllers/errors.server.controller'
));
const { Op } = require('sequelize');

/**
 * show the profile
 */
exports.show = async function (req, res) {
  try {
    const profile = await Profile.findOne({ where: { id: 1 } });
    return res.status(200).json({ data: profile });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * Read one profile
 */
exports.read = async function (req, res) {
  const id = req.params.id;

  try {
    const profile = await Profile.findOne({ where: { id: id } });

    //check if journey not with id params not found
    if (!profile) {
      return res.status(404).send({
        message: `Profile with id ${id} is not found`,
      });
    }

    return res.status(200).json({
      data: profile,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * update profile
 */
exports.update = async function (req, res) {
  const id = req.params.id;
  const payload = req.body;

  try {
    const profile = await Profile.findOne({ where: { id: id } });

    //check if journey not with id params not found
    if (!profile) {
      return res.status(404).send({
        message: `Profile with id ${id} is not found`,
      });
    }

    await profile.update(payload);

    return res.status(200).json({
      message: 'Profile successfully edited',
      data: profile,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};
