'use strict';

/**
 * Module dependencies
 */
const path = require('path');
const profileController = require('../controller/profile.server.controller');
const policy = require(path.resolve('./config/lib/acl_auth'));

/**
 * Routes
 */
module.exports = function (app) {
  //route for cms
  app.route('/api/profile').all(policy.isAllowed).get(profileController.show);

  app
    .route('/api/profile/:id')
    .all(policy.isAllowed)
    .get(profileController.read)
    .put(profileController.update);

  app.route('/api/public/profile').get(profileController.show);
};
