'use strict';

/**
 * Module dependencies
 */

const path = require('path');
const db = require(path.resolve('./config/lib/sequelize'));
const Setting = db.Setting;
const errorHandler = require(path.resolve(
  './modules/core/controllers/errors.server.controller'
));
const { Op } = require('sequelize');

/**
 * list of setting
 */
exports.list = async function (req, res) {
  try {
    const setting = await Setting.findAll({
      attributes: ['optionName', 'optionValue'],
    });
    return res.status(200).json({
      data: setting,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

exports.update = async function (req, res) {
  const { isEnableEnquiries, recipientEmail, senderEmail } = req.body;
  try {
    const settingIsEnableEnquiries = await Setting.findOne({
      where: { optionName: 'isEnableEnquiries' },
    });

    const settingRecipientEmail = await Setting.findOne({
      where: { optionName: 'recipientEmail' },
    });

    const settingSenderEmail = await Setting.findOne({
      where: { optionName: 'senderEmail' },
    });

    await settingIsEnableEnquiries.update({
      optionValue: isEnableEnquiries.toString(),
    });

    await settingRecipientEmail.update({ optionValue: recipientEmail });

    await settingSenderEmail.update({ optionValue: senderEmail });

    const setting = await Setting.findAll({
      attributes: ['optionName', 'optionValue'],
    });

    return res.status(200).json({
      message: 'Settings has been saved successfully',
      data: setting,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};
