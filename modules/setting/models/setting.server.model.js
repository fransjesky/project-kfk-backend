'use strict';

/**
 * Config Schema
 */
module.exports = function (sequelize, DataTypes) {
  const Setting = sequelize.define('Setting', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    optionName: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null,
    },
    optionValue: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null,
    },
  });
  return Setting;
};
