'use strict';

/**
 * Module dependencies
 */

const settingController = require('../controller/setting.server.controller');

/**
 * Routes
 */
module.exports = function (app) {
  //route for cms
  app
    .route('/api/setting')
    .get(settingController.list)
    .put(settingController.update);
};
