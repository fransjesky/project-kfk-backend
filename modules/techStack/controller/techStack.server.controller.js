'use strict';

/**
 * Module dependencies
 */

const path = require('path');
const db = require(path.resolve('./config/lib/sequelize'));
const TechStack = db.TechStack;
const errorHandler = require(path.resolve(
  './modules/core/controllers/errors.server.controller'
));
const { Op } = require('sequelize');

/**
 * list of Tech stack
 */
exports.list = async function (req, res) {
  try {
    const { keyword, category, limit = 5, offset = 0 } = req.query;

    let query = {
      where: {
        [Op.and]: [],
      },
      order: [
        ['category', 'asc'],
        ['order', 'asc'],
      ],
      limit,
      offset,
      attributes: ['id', 'name', 'logo', 'category', 'order'],
    };

    if (keyword) {
      query.where[Op.and].push({ name: { [Op.iLike]: `%${keyword}%` } });
    }

    if (category) {
      query.where[Op.and].push({ category: category });
    }

    const techStacks = await TechStack.findAndCountAll(query);
    return res.status(200).json({
      count: techStacks.count,
      data: techStacks.rows,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

exports.publicList = async function (req, res) {
  const { category } = req.query;
  let query = {
    attributes: ['id', 'logo', 'category', 'order'],
    order: [['order', 'asc']],
    where: {
      [Op.and]: [],
    },
  };

  if (category) {
    query.where[Op.and].push({ category: category });
  }

  try {
    const techStacks = await TechStack.findAll(query);

    return res.status(200).json({
      data: techStacks,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * create Tech stack
 */
exports.create = async function (req, res) {
  const payload = req.body;
  try {
    const techStack = await TechStack.create(payload);
    return res.json({
      message: 'A new tech has been Added successfully',
      data: techStack,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * show one tech stack
 */
exports.read = async function (req, res) {
  const id = req.params.id;

  try {
    const techStack = await TechStack.findOne({ where: { id } });

    //check if techStack not with id params not found
    if (!techStack) {
      return res.status(400).send({
        message: `Tech Stack with id ${id} is not found`,
      });
    }

    return res.status(200).json({
      data: techStack,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * update tech stack
 */
exports.update = async function (req, res) {
  const id = req.params.id;
  const { name, logo, order, category } = req.body;

  try {
    //query
    const techStack = await TechStack.findOne({ where: { id } });

    //check if techStack not with id params not found
    if (!techStack) {
      return res.status(400).send({
        message: `techStack with id ${id} is not found`,
      });
    }

    const techStackWithMaxOrder = await TechStack.findOne({
      order: [['order', 'desc']],
      attributes: ['order'],
      where: {
        category:
          category === techStack.category ? category : techStack.category,
      },
    });

    const maxOrder = techStackWithMaxOrder ? techStackWithMaxOrder.order : 1;

    if (order != techStack.order) {
      //cek if category dont change
      if (category === techStack.category) {
        const maxBetween = order > techStack.order ? techStack.order : maxOrder;
        //get all tech stack by category
        const techStacks = await TechStack.findAll({
          attributes: ['id', 'order'],
          where: {
            category: techStack.category,
            order: {
              [Op.between]:
                order < maxBetween ? [order, maxBetween] : [maxBetween, order],
            },
          },
        });

        //loop for update each item in order
        techStacks.forEach(async (item) => {
          //chek if order update with old order
          if (order > techStack.order) {
            await item.update({
              order: item.order <= order ? item.order - 1 : item.order,
            });
          } else {
            await item.update({
              order:
                item.order <= techStack.order ? item.order + 1 : item.order,
            });
          }
        });
      } else {
        const maxBetween = order > techStack.order ? techStack.order : maxOrder;

        const techStacks = await TechStack.findAll({
          attributes: ['id', 'order'],
          where: {
            category: techStack.category,
            order: {
              [Op.between]:
                order < maxBetween ? [order, maxBetween] : [maxBetween, order],
            },
          },
        });

        techStacks.forEach((item) => {
          if (item.order > techStack.order) {
            item.update({
              order: item.order - 1,
            });
          }
        });
      }
    }

    await techStack.update({
      name,
      logo,
      category,
      order,
    });

    return res.status(200).json({
      message: `${techStack.name} has been edited successfully`,
      data: techStack,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * delete teach stack
 */

exports.delete = async function (req, res) {
  const id = req.params.id;

  try {
    const techStack = await TechStack.findOne({ where: { id } });

    //check if TechStack not with id params not found
    if (!techStack) {
      return res.status(400).send({
        message: `Tech Stack with id ${id} is not found`,
      });
    }

    const techStackWithMaxOrder = await TechStack.findOne({
      order: [['order', 'desc']],
      attributes: ['order'],
      where: {
        category: techStack.category,
      },
    });

    await TechStack.findAll({
      attributes: ['id', 'order'],
      where: {
        category: techStack.category,
        order: {
          [Op.between]: [techStack.order, techStackWithMaxOrder.order],
        },
      },
    }).then(function (result) {
      if (result) {
        result.forEach((item) => {
          item.update({
            order: item.order - 1,
          });
        });
      }
    });

    await techStack.destroy();

    return res.status(200).json({
      message: `${techStack.name} has been deleted successfully`,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

exports.showOrder = async function (req, res) {
  try {
    const { category } = req.query;

    if (!category) {
      return res.status(400).send({
        message: 'category is required',
      });
    }

    const techStack = await TechStack.findOne({
      order: [['order', 'desc']],
      attributes: ['order'],
      where: {
        category,
      },
    });

    const lastOrderInTechStack = techStack ? techStack.order + 1 : 1;

    let data = [];

    for (let i = 1; i <= lastOrderInTechStack; i++) {
      data.push(i);
    }

    return res.status(200).json({
      data,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};
