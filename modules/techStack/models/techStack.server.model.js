'use strict';

/**
 * TechStack Schema
 */

module.exports = function (sequelize, DataTypes) {
  const TechStack = sequelize.define('TechStack', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a name',
        },
      },
    },
    logo: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a logo',
        },
      },
    },
    category: {
      type: DataTypes.ENUM('database', 'techStack', 'cyberSecurity', 'product'),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a order',
        },
        isIn: [['database', 'techStack', 'cyberSecurity', 'product']],
      },
    },
    order: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a order',
        },
        isInt: {
          msg: 'Must be an integer number',
        },
      },
    },
  });

  return TechStack;
};
