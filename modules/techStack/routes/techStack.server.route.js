'use strict';

/**
 * Module dependencies
 */
const path = require('path');
const techStackController = require('../controller/techStack.server.controller');
const policy = require(path.resolve('./config/lib/acl_auth'));

/**
 * Routes
 */
module.exports = function (app) {
  //route for cms
  app
    .route('/api/tech-stack')
    .all(policy.isAllowed)
    .get(techStackController.list)
    .post(techStackController.create);

  app.route('/api/tech-stack/number').get(techStackController.showOrder);

  app
    .route('/api/tech-stack/:id')
    .all(policy.isAllowed)
    .get(techStackController.read)
    .put(techStackController.update)
    .delete(techStackController.delete);

  app.route('/api/public/tech-stack').get(techStackController.publicList);
};
