'use strict';

/**
 * Module dependencies
 */

const path = require('path');
const db = require(path.resolve('./config/lib/sequelize'));
const Testimony = db.Testimony;
const errorHandler = require(path.resolve(
  './modules/core/controllers/errors.server.controller'
));
const { Op } = require('sequelize');

/**
 * list of testimony
 */
exports.list = async function (req, res) {
  try {
    const {
      keyword,
      sort = 'createdAt',
      order = 'ASC',
      limit = 5,
      offset = 0,
    } = req.query;

    let query = {
      order: [[sort, order]],
      limit,
      offset,
    };

    if (keyword) {
      query.where = {
        [Op.and]: {
          name: { [Op.iLike]: `%${keyword}%` },
        },
      };
    }

    const testimonies = await Testimony.findAndCountAll(query);
    return res.status(200).json({
      count: testimonies.count,
      data: testimonies.rows,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

exports.publicList = async function (req, res) {
  try {
    const testimonies = await Testimony.findAll({
      attributes: [
        'id',
        'name',
        'position',
        'companyName',
        'clientTestimony',
        'photo',
      ],
      order: [['createdAt', 'ASC']],
    });

    return res.status(200).json({
      data: testimonies,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * create testimony
 */
exports.create = async function (req, res) {
  const payload = req.body;
  try {
    const testimony = await Testimony.create(payload);
    return res.json({
      message: 'A new Testimony has been Added successfully',
      data: testimony,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * show one testimony
 */
exports.read = async function (req, res) {
  const id = req.params.id;

  try {
    const testimony = await Testimony.findOne({ where: { id } });

    //check if testimony not with id params not found
    if (!testimony) {
      return res.status(400).send({
        message: `Testimony with id ${id} is not found`,
      });
    }

    return res.status(200).json({
      data: testimony,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * update testimony
 */
exports.update = async function (req, res) {
  const id = req.params.id;
  const payload = req.body;

  try {
    //query
    const testimony = await Testimony.findOne({ where: { id } });

    //check if testimony not with id params not found
    if (!testimony) {
      return res.status(400).send({
        message: `testimony with id ${id} is not found`,
      });
    }

    await testimony.update(payload);

    return res.status(200).json({
      message: `${testimony.name} testimony has been edited successfully`,
      data: testimony,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * delete testimony
 */
exports.delete = async function (req, res) {
  const id = req.params.id;

  try {
    const testimony = await Testimony.findOne({ where: { id } });

    //check if Testimony not with id params not found
    if (!testimony) {
      return res.status(400).send({
        message: `testimony with id ${id} is not found`,
      });
    }

    await testimony.destroy();

    return res.status(200).json({
      message: `${testimony.name} testimony has been deleted successfully`,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};
