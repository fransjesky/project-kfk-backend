'use strict';

/**
 * Module dependencies
 */

const path = require('path');
const testimonyController = require('../controller/testimony.server.controller');
const policy = require(path.resolve('./config/lib/acl_auth'));

/**
 * Routes
 */
module.exports = function (app) {
  //route for cms
  app
    .route('/api/testimony')
    .all(policy.isAllowed)
    .get(testimonyController.list)
    .post(testimonyController.create);

  app
    .route('/api/testimony/:id')
    .all(policy.isAllowed)
    .get(testimonyController.read)
    .put(testimonyController.update)
    .delete(testimonyController.delete);

  app.route('/api/public/testimony').get(testimonyController.publicList);
};
