'use strict';

/**
 * module dependency
 */
const path = require('path');
const errorHandler = require(path.resolve(
  './modules/core/controllers/errors.server.controller'
));

exports.upload = async function (req, res) {
  try {
    const { originalname, Location } = req.file;
    return res.status(200).json({
      message: 'Upload has been successfully',
      data: {
        name: originalname,
        location: Location,
      },
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
      data: null,
    });
  }
};
