'use strict';

/**
 * Module dependencies
 */
const path = require('path');
const uploadController = require('../controller/upload.server.controller');
const uploadFile = require(path.resolve('./config/lib/upload'));

/**
 * Routes
 */
module.exports = function (app) {
  app.route('/api/upload').post(uploadFile, uploadController.upload);
};
