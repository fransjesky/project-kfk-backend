'use strict';

/**
 * Module dependencies
 */
const path = require('path'),
  db = require(path.resolve('./config/lib/sequelize')),
  Role = db.Role,
  errorHandler = require(path.resolve(
    './modules/core/controllers/errors.server.controller'
  )),
  { Op } = require('sequelize'),
  _ = require('lodash'),
  sequelize = require('sequelize');

/**
 * list role
 */
exports.list = async function (req, res) {
  try {
    const {
      keyword,
      sort = 'createdAt',
      order = 'ASC',
      limit = 5,
      offset = 0,
    } = req.query;

    let query = {
      where: {
        [Op.and]: [],
      },
      order: [[sort, order]],
      limit,
      offset,
      include: [
        {
          model: db.User,
          attributes: [],
        },
        {
          model: db.Acl,
          attributes: ['id', 'name'],
        },
      ],
      attributes: [
        'id',
        'name',
        'createdAt',
        [
          sequelize.literal(
            `(SELECT COUNT(*) FROM "Users" where "Users"."RoleId" = "Role"."id")::integer`
          ),
          'CountUser',
        ],
      ],
      distinct: true,
    };

    if (keyword) {
      query.where[Op.and].push({ name: { [Op.iLike]: `%${keyword}%` } });
    }

    const roles = await Role.findAndCountAll(query);

    return res.status(200).json({
      count: roles.count,
      data: roles.rows,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * list master menu
 */
exports.listMasterMenu = async function (req, res) {
  try {
    const masterMenu = await db.MasterMenu.findAll({
      attributes: ['id', 'title'],
      where: {
        type: null,
        id: {
          [Op.notIn]: [9],
        },
      },
    });

    return res.json({
      data: masterMenu,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

exports.listAcl = async function (req, res) {
  try {
    const acls = await db.Acl.findAll({
      attributes: ['id', 'name', 'module', 'action'],
    });

    const newAcls = _.chain(acls).groupBy('module');

    return res.json({
      data: newAcls,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * Create an userRole
 */
exports.create = async function (req, res, next) {
  const { masterMenuIds, aclIds, name } = req.body;

  try {
    //get menu with parent
    const menus = await db.MasterMenu.findAll({
      where: {
        id: { [Op.in]: masterMenuIds },
        parentId: { [Op.not]: null },
      },
    });

    //get parent id & unique
    const idsMenu = menus
      .map((menu) => menu.parentId)
      .filter((menuId, index, array) => array.indexOf(menuId) === index);

    const masterMenus = await db.MasterMenu.findAll({
      where: { id: { [Op.in]: [...masterMenuIds, ...idsMenu] } },
    });

    const acls = await db.Acl.findAll({
      where: { id: { [Op.in]: aclIds } },
    });

    const role = await db.Role.create({ name });
    await role.addAcl(acls); // save reatation role and acl
    await role.addMasterMenu(masterMenus); // save reatation role and menu

    return res.json({
      message: 'Role has been Added successfully',
      data: role,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e.message),
    });
  }
};

/**
 * Show the current userRole
 */
exports.read = async function (req, res) {
  const id = req.params.id;
  try {
    const role = await db.Role.findOne({
      where: {
        id,
      },
      include: [
        { model: db.Acl, attributes: ['id'], required: false },
        {
          model: db.MasterMenu,
          attributes: ['id'],
          where: {
            type: null,
          },
          required: false,
        },
      ],
    });

    if (!role) {
      return res.status(400).send({
        message: `role with id ${id} is not found`,
      });
    }

    const masterMenuIds = role.MasterMenus.map((masterMenu) => masterMenu.id);
    const aclIds = role.Acls.map((acl) => acl.id);

    delete role.dataValues.MasterMenus;
    delete role.dataValues.Acls;

    role.dataValues.masterMenuIds = masterMenuIds;
    role.dataValues.aclIds = aclIds;

    return res.json({
      data: role,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * Update an userRole
 */
exports.update = async function (req, res) {
  const id = req.params.id;
  const { masterMenuIds, aclIds, name } = req.body;

  try {
    const role = await db.Role.findOne({ where: { id } });

    //get menu with parent
    const menus = await db.MasterMenu.findAll({
      where: {
        id: { [Op.in]: masterMenuIds },
        parentId: { [Op.not]: null },
      },
    });

    //get parent id & unique
    const idsMenu = menus
      .map((menu) => menu.parentId)
      .filter((menuId, index, array) => array.indexOf(menuId) === index);

    const masterMenus = await db.MasterMenu.findAll({
      where: { id: { [Op.in]: [...masterMenuIds, ...idsMenu] } },
    });

    const acls = await db.Acl.findAll({
      where: { id: { [Op.in]: aclIds } },
    });

    await db.RoleMenu.destroy({ where: { roleId: id } });
    await db.AclRole.destroy({ where: { roleId: id } });

    await role.addMasterMenu(masterMenus);
    await role.addAcl(acls);
    await role.update({ name: name });

    return res.json({
      message: 'Role has been edited successfully',
      data: role,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * Delete an userRole
 */
exports.delete = async function (req, res) {
  const id = req.params.id;

  try {
    const role = await Role.findOne({
      where: { id },
      include: {
        model: db.User,
      },
    });

    //check if Role not with id params not found
    if (!role) {
      return res.status(400).send({
        message: `role with id ${id} is not found`,
      });
    }

    //check if role have any users
    if (role.Users.length > 0) {
      return res.status(400).send({
        message: `There are still ${role.Users.length} users using this role`,
      });
    }

    await role.destroy();

    return res.status(200).json({
      message: 'Role has been deleted successfully',
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};
