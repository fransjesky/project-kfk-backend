'use strict';

module.exports = function (sequelize, DataTypes) {
  const Role = sequelize.define('Role', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING,
      unique: true,
    },
  });
  Role.associate = function (models) {
    Role.hasMany(models.User);
    Role.belongsToMany(models.Acl, {
      through: 'AclRole',
      foreignKey: 'roleId',
    });
    Role.belongsToMany(models.MasterMenu, {
      through: 'RoleMenu',
      foreignKey: 'roleId',
    });
  };

  return Role;
};
