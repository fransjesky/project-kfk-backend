'use strict';

/**
 * Module dependencies
 */
const path = require('path');
const policy = require(path.resolve('./config/lib/acl_auth'));
const userRoles = require('../controllers/user-roles.server.controller');

module.exports = function (app) {
  // UserRoles collection routes
  app
    .route('/api/user-roles')
    .all(policy.isAllowed)
    .get(userRoles.list)
    .post(userRoles.create);

  app.route('/api/user-roles/master-menu').get(userRoles.listMasterMenu);
  app.route('/api/user-roles/acl').get(userRoles.listAcl);

  app
    .route('/api/user-roles/:id')
    // .all(policy.isAllowed)
    .get(userRoles.read)
    .put(userRoles.update)
    .delete(userRoles.delete);
};
