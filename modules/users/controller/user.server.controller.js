'use strict';

/**
 * Module dependencies
 */

const path = require('path');
const db = require(path.resolve('./config/lib/sequelize'));
const User = db.User;
const errorHandler = require(path.resolve(
  './modules/core/controllers/errors.server.controller'
));
const { Op } = require('sequelize');

/**
 * list of User
 */
exports.list = async function (req, res) {
  try {
    const {
      keyword,
      sort = 'createdAt',
      order = 'ASC',
      limit = 5,
      offset = 0,
    } = req.query;

    let query = {
      where: {
        [Op.and]: [],
      },
      order: [[sort, order]],
      limit,
      offset,
      attributes: ['id', 'name', 'email', 'photo', 'createdAt'],
      include: [
        {
          model: db.Role,
          attributes: ['name'],
        },
      ],
    };

    if (keyword) {
      query.where[Op.and].push({ name: { [Op.iLike]: `%${keyword}%` } });
    }

    const users = await User.findAndCountAll(query);

    return res.status(200).json({
      count: users.count,
      data: users.rows,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * create User
 */
exports.create = async function (req, res) {
  try {
    const payload = req.body;
    const user = await User.create(payload);

    delete user.dataValues.password;
    delete user.dataValues.salt;

    return res.status(200).json({
      message: 'User has beed created',
      data: user,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * show one User
 */
exports.read = async function (req, res) {
  const id = req.params.id;

  try {
    const user = await User.findOne({
      where: { id },
      include: [
        {
          model: db.Role,
          attributes: ['id', 'name'],
        },
      ],
      attributes: ['id', 'name', 'email', 'photo'],
    });

    if (!user) {
      return res.status(400).send({
        message: `User with id ${id} is not found`,
      });
    }

    return res.status(200).json({
      data: user,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * update User
 */
exports.update = async function (req, res) {
  const id = req.params.id;

  try {
    const user = await User.findOne({
      where: { id },
    });

    if (!user) {
      return res.status(400).send({
        message: `User with id ${id} is not found`,
      });
    }

    if (req.body.password !== '') {
      await user.update({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        RoleId: req.body.RoleId,
        salt: null,
      });
    }

    await user.update({
      name: req.body.name,
      email: req.body.email,
      RoleId: req.body.RoleId,
    });

    delete user.dataValues.password;
    delete user.dataValues.salt;

    return res.status(200).json({
      message: 'User has been edited successfully',
      data: user,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

/**
 * delete User
 */
exports.delete = async function (req, res) {
  const id = req.params.id;

  try {
    const user = await User.findOne({
      where: { id },
    });

    if (!user) {
      return res.status(400).send({
        message: `User with id ${id} is not found`,
      });
    }

    await user.destroy();

    return res.status(200).json({
      message: 'User has been deleted successfully',
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};

exports.listRole = async function (req, res) {
  try {
    const roles = await db.Role.findAll({
      order: [['createdAt', 'asc']],
      attributes: ['id', 'name'],
    });
    return res.status(200).json({
      data: roles,
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};
