'use strict';

/**
 * Module dependencies
 */
const crypto = require('crypto');

/**
 * Support Function
 */
const hashPassword = (user, password) => {
  if (!password) {
    password = user.dataValues.password;
  }

  if (user.dataValues.salt && user.dataValues.password) {
    return crypto
      .pbkdf2Sync(
        password,
        Buffer.from(user.dataValues.salt, 'base64'),
        10000,
        64,
        'sha1'
      )
      .toString('base64');
  } else {
    return password;
  }
};

const checkPassword = (user) => {
  if (user.password.length >= 8 && user.password <= 20) {
    throw new Error('Your password should be between 8-20 characters!');
  } else {
    user.dataValues.salt = crypto.randomBytes(16).toString('base64');
    user.dataValues.password = hashPassword(user);
  }
};

/**
 * User Schema
 */
module.exports = function (sequelize, DataTypes) {
  const User = sequelize.define(
    'User',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: DataTypes.STRING(255),
        allowNull: false,
        validate: {
          notNull: {
            msg: 'Please input a name',
          },
        },
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: {
          msg: 'This email is already taken.',
        },
        validate: {
          isEmail: {
            args: true,
            msg: 'Email format is invalid',
          },
          notEmpty: true,
        },
        set(val) {
          this.setDataValue('email', val.toLowerCase());
        },
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      salt: DataTypes.STRING,
      photo: DataTypes.TEXT,
    },
    {
      hooks: {
        beforeCreate: (user, option) => {
          if (user.dataValues.password) {
            checkPassword(user);
          }
        },
        beforeUpdate: (user, option) => {
          if (user.dataValues.password) {
            checkPassword(user);
          }
        },
      },
    }
  );

  User.prototype.authenticate = (user, password) => {
    return user.dataValues.password === hashPassword(user, password);
  };

  User.associate = (models) => {
    User.belongsTo(models.Role);
  };

  return User;
};
