'use strict';

/**
 * Module dependencies
 */
const path = require('path');
const userController = require('../controller/user.server.controller');
const policy = require(path.resolve('./config/lib/acl_auth'));

/**
 * Routes
 */
module.exports = function (app) {
  //route for cms
  app
    .route('/api/user')
    .all(policy.isAllowed)
    .get(userController.list)
    .post(userController.create);
  app.route('/api/user/role').get(userController.listRole);

  app
    .route('/api/user/:id')
    .all(policy.isAllowed)
    .get(userController.read)
    .put(userController.update)
    .delete(userController.delete);
};
