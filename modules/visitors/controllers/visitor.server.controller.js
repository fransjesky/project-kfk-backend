'use strict';

/**
 * Module dependencies
 */
const path = require('path'),
  db = require(path.resolve('./config/lib/sequelize')),
  Visitor = db.Visitor,
  errorHandler = require(path.resolve(
    './modules/core/controllers/errors.server.controller'
  )),
  { Op } = require('sequelize');

exports.create = async function (req, res) {
  const payload = req.body;
  try {
    await Visitor.create(payload);

    return res.status(200).json({
      message: 'success',
    });
  } catch (e) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(e),
    });
  }
};
