'use strict';

/**
 * visitor Schema
 */

module.exports = function (sequelize, DataTypes) {
  const Visitor = sequelize.define('Visitor', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    ip: {
      type: DataTypes.STRING(255),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a ip',
        },
      },
    },
    path: {
      type: DataTypes.STRING(255),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please input a path',
        },
      },
    },
  });

  return Visitor;
};
