'use strict';

/**
 * Module dependencies
 */
const visitorController = require('../controllers/visitor.server.controller');

module.exports = function (app) {
  app.route('/api/public/visitor').post(visitorController.create);
};
