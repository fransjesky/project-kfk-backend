'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let acl = [
      //patner
      {
        name: 'List Partners',
        module: 'Partners',
        action: 'list',
        resource: '/api/patner',
        permission: 'get',
        description: 'Permission access for see all of partner',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Add new Partner',
        module: 'Partners',
        action: 'create',
        resource: '/api/patner',
        permission: 'post',
        description: 'Permission access for add new partner',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Read Partner',
        module: 'Partners',
        action: 'read',
        resource: '/api/patner/:id',
        permission: 'get',
        description:
          'Permission access for see one of the partner that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Update Partner',
        module: 'Partners',
        action: 'update',
        resource: '/api/patner/:id',
        permission: 'put',
        description:
          'Permission access for update one of the partner that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Delete Partner',
        module: 'Partners',
        action: 'delete',
        resource: '/api/patner/:id',
        permission: 'delete',
        description:
          'Permission access for delete one of the partner that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },

      //company profile
      {
        name: 'List Company Profile',
        module: 'Company-Profile',
        action: 'list',
        resource: '/api/profile',
        permission: 'get',
        description: 'Permission access for see company profile',
        createdAt: new Date(),
        updatedAt: new Date(),
      },

      {
        name: 'Read Company Profile',
        module: 'Company-Profile',
        action: 'read',
        resource: '/api/profile/:id',
        permission: 'get',
        description: 'Permission access for see detail company profile',
        createdAt: new Date(),
        updatedAt: new Date(),
      },

      {
        name: 'Update Company Profile',
        module: 'Company-Profile',
        action: 'update',
        resource: '/api/profile/:id',
        permission: 'put',
        description: 'Permission access for update company profile',
        createdAt: new Date(),
        updatedAt: new Date(),
      },

      //our journey
      {
        name: 'List Journey',
        module: 'Journeys',
        action: 'list',
        resource: '/api/journey',
        permission: 'get',
        description: 'Permission access for see all of journey',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Add new Journey',
        module: 'Journeys',
        action: 'create',
        resource: '/api/journey',
        permission: 'post',
        description: 'Permission access for add new journey',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Read Journey',
        module: 'Journeys',
        action: 'read',
        resource: '/api/journey/:id',
        permission: 'get',
        description:
          'Permission access for see one of the journey that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Update Journey',
        module: 'Journeys',
        action: 'update',
        resource: '/api/journey/:id',
        permission: 'put',
        description:
          'Permission access for update one of the journey that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Delete Journey',
        module: 'Journeys',
        action: 'delete',
        resource: '/api/journey/:id',
        permission: 'delete',
        description:
          'Permission access for delete one of the journey that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },

      //tech stack
      {
        name: 'List Tech Stack',
        module: 'Tech-Stack',
        action: 'list',
        resource: '/api/tech-stack',
        permission: 'get',
        description: 'Permission access for see all of tech stack',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Add new Tech Stack',
        module: 'Tech-Stack',
        action: 'create',
        resource: '/api/tech-stack',
        permission: 'post',
        description: 'Permission access for add new tech stack',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Read Tech Stack',
        module: 'Tech-Stack',
        action: 'read',
        resource: '/api/tech-stack/:id',
        permission: 'get',
        description:
          'Permission access for see one of the Tech Stack that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Update Tech Stack',
        module: 'Tech-Stack',
        action: 'update',
        resource: '/api/tech-stack/:id',
        permission: 'put',
        description:
          'Permission access for update one of the Tech Stack that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Delete Tech',
        module: 'Tech-Stack',
        action: 'delete',
        resource: '/api/tech-stack/:id',
        permission: 'delete',
        description:
          'Permission access for delete one of the Tech Stack that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },

      //our team
      {
        name: 'List Members',
        module: 'Members',
        action: 'list',
        resource: '/api/member',
        permission: 'get',
        description: 'Permission access for see all of Member',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Add new Member',
        module: 'Members',
        action: 'create',
        resource: '/api/member',
        permission: 'post',
        description: 'Permission access for add new member',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Read Member',
        module: 'Members',
        action: 'read',
        resource: '/api/member/:id',
        permission: 'get',
        description:
          'Permission access for see one of the Member that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Update Member',
        module: 'Members',
        action: 'update',
        resource: '/api/member/:id',
        permission: 'put',
        description:
          'Permission access for update of the Member that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Delete Member',
        module: 'Members',
        action: 'delete',
        resource: '/api/member/:id',
        permission: 'delete',
        description:
          'Permission access for delete of the Member that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },

      //Our Portofolios
      {
        name: 'List Portfolio',
        module: 'Portfolio',
        action: 'list',
        resource: '/api/portfolio',
        permission: 'get',
        description: 'Permission access for see all of Portfolio',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Add new Portfolio',
        module: 'Portfolio',
        action: 'create',
        resource: '/api/portfolio',
        permission: 'post',
        description: 'Permission access for add new Portfolio',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Read Portfolio',
        module: 'Portfolio',
        action: 'read',
        resource: '/api/portfolio/:id',
        permission: 'get',
        description:
          'Permission access for see one of the Portfolio that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Update Portfolio',
        module: 'Portfolio',
        action: 'update',
        resource: '/api/portfolio/:id',
        permission: 'put',
        description:
          'Permission access for update of the Portfolio that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Delete Portfolio',
        module: 'Portfolio',
        action: 'delete',
        resource: '/api/portfolio/:id',
        permission: 'delete',
        description:
          'Permission access for delete of the Portfolio that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },

      //Our Testimonies
      {
        name: 'List Testimony',
        module: 'Testimonies',
        action: 'list',
        resource: '/api/testimony',
        permission: 'get',
        description: 'Permission access for see all of Testimony',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Add new Testimony',
        module: 'Testimonies',
        action: 'create',
        resource: '/api/testimony',
        permission: 'post',
        description: 'Permission access for add new Testimony',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Read Testimony',
        module: 'Testimonies',
        action: 'read',
        resource: '/api/testimony/:id',
        permission: 'get',
        description:
          'Permission access for see one of the Testimony that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Update Testimony',
        module: 'Testimonies',
        action: 'update',
        resource: '/api/testimony/:id',
        permission: 'put',
        description:
          'Permission access for update of the Portfolio that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Delete Testimony',
        module: 'Testimonies',
        action: 'delete',
        resource: '/api/testimony/:id',
        permission: 'delete',
        description:
          'Permission access for delete of the Portfolio that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },

      //Careers
      {
        name: 'List Job vacancy',
        module: 'Careers',
        action: 'list',
        resource: '/api/career',
        permission: 'get',
        description: 'Permission access for see all of Job Vacancy',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Add Job vacancy',
        module: 'Careers',
        action: 'create',
        resource: '/api/career',
        permission: 'post',
        description: 'Permission access for add new Job Vacancy',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Read Job vacancy',
        module: 'Careers',
        action: 'read',
        resource: '/api/career/:id',
        permission: 'get',
        description:
          'Permission access for see one of the Job Vacancy that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Update Job vacancy',
        module: 'Careers',
        action: 'update',
        resource: '/api/career/:id',
        permission: 'put',
        description:
          'Permission access for Update one of the Job Vacancy that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Delete Job vacancy',
        module: 'Careers',
        action: 'delete',
        resource: '/api/career/:id',
        permission: 'delete',
        description:
          'Permission access for delete of the Job Vacancy that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },

      //Roles Management
      {
        name: 'List Role',
        module: 'User-roles',
        action: 'list',
        resource: '/api/user-roles',
        permission: 'get',
        description: 'Permission access for see all of Role',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Add new Role',
        module: 'User-roles',
        action: 'create',
        resource: '/api/user-roles',
        permission: 'post',
        description: 'Permission access for add new Role',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Read Role',
        module: 'User-roles',
        action: 'read',
        resource: '/api/user-roles/:id',
        permission: 'get',
        description:
          'Permission access for see one of the Role that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Update Role',
        module: 'User-roles',
        action: 'update',
        resource: '/api/user-roles/:id',
        permission: 'put',
        description:
          'Permission access for Update one of the Role that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Delete Role',
        module: 'User-roles',
        action: 'delete',
        resource: '/api/user-roles/:id',
        permission: 'delete',
        description:
          'Permission access for delete one of the Role that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },

      //User Management
      {
        name: 'List User',
        module: 'Users',
        action: 'list',
        resource: '/api/user',
        permission: 'get',
        description: 'Permission access for see all of Role',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Add new User',
        module: 'Users',
        action: 'create',
        resource: '/api/user',
        permission: 'post',
        description: 'Permission access for add new Role',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Read User',
        module: 'Users',
        action: 'read',
        resource: '/api/user/:id',
        permission: 'get',
        description:
          'Permission access for see one of the User that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Update User',
        module: 'Users',
        action: 'update',
        resource: '/api/user/:id',
        permission: 'put',
        description:
          'Permission access for update one of the User that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Delete User',
        module: 'Users',
        action: 'delete',
        resource: '/api/user/:id',
        permission: 'delete',
        description:
          'Permission access for delete one of the User that has been selected',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ];

    await queryInterface.bulkInsert('Acls', acl);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Acls', null, {});
    return;
  },
};
