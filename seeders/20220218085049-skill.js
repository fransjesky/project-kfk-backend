'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const skills = [
      {
        name: 'UI Design',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'UX Design',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'UX Research',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Design Thinking',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Graphic Design',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Figma',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'InVision',
        type: 'additional',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'English',
        type: 'additional',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'HTML',
        type: 'hybrid',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'CSS',
        type: 'hybrid',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Javascript',
        type: 'hybrid',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'ReactJs',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'NodeJs',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },

      {
        name: 'ExpressJs',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'MongoDB',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'PostgreSQL',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'NextJs',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'CI/CD',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'REST API',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Git',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Redux',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Mobs',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Flux',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'AWS',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Leadership Skills',
        type: 'additional',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Analytical Skills',
        type: 'additional',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Problem-Solving Skills',
        type: 'additional',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Good communication',
        type: 'additional',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Interpersonal Skills',
        type: 'additional',
        createdAt: new Date(),
        updatedAt: new Date(),
      },

      {
        name: 'Team Player',
        type: 'additional',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Mocha',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Jest',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Mentoring Skills',
        type: 'additional',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Coaching Skills',
        type: 'additional',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Data Oriented',
        type: 'hybrid',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'IoT',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Attention to detail',
        type: 'additional',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Android',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'iOS',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Verbal Communication Skills',
        type: 'additional',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Written Communication Skills',
        type: 'additional',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Software QA',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Test Plan',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Data Analysis',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Marketing & Sales',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Business Development',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'B2B',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Budgeting',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Public Relations',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Cybersecurity',
        type: 'hybrid',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Market Research',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Agile',
        type: 'hybrid',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Scrum',
        type: 'hybrid',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'QTP',
        type: 'hybrid',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Automation',
        type: 'main',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ];

    await queryInterface.bulkInsert('Skills', skills);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Skills', null, {});
    return;
  },
};
