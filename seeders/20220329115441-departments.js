'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Departments', [
      {
        name: 'The Artisans',
        color: '#C979EE',
        logo: 'https://s3.ap-southeast-1.amazonaws.com/project-xcidic4.0-bucket/assets/department/artisan.svg',
        description: 'The evaluate brain, ensure quality and identify issues.',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'The Coders',
        color: '#00A3FF',
        logo: 'https://s3.ap-southeast-1.amazonaws.com/project-xcidic4.0-bucket/assets/department/coders.svg',
        description: 'The technical brain, bringing lines of code to life.',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'The Testers',
        color: '#32BABA',
        logo: 'https://s3.ap-southeast-1.amazonaws.com/project-xcidic4.0-bucket/assets/department/artisan.svg',
        description: 'The evaluate brain, ensure quality and identify issues.',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'The Project Owners',
        color: '#320B85',
        logo: 'https://s3.ap-southeast-1.amazonaws.com/project-xcidic4.0-bucket/assets/department/project_owner.svg',
        description: 'The evaluate brain, ensure quality and identify issues.',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Human Resources',
        color: '#FF9D42',
        logo: 'https://s3.ap-southeast-1.amazonaws.com/project-xcidic4.0-bucket/assets/department/human_resource.svg',
        description:
          'Manage, and collaborate with the teams to create a healthy enviroment',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Business Development',
        color: '#1B1B1B',
        logo: 'https://s3.ap-southeast-1.amazonaws.com/project-xcidic4.0-bucket/assets/department/businness_development.svg',
        description:
          ' The ideas, initiatives, and activities that help make a business better',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Finance, Accounting & Tax',
        color: '#FF4259',
        logo: 'https://s3.ap-southeast-1.amazonaws.com/project-xcidic4.0-bucket/assets/department/Finance-Accounting-Tax.svg',
        description:
          'Acquiring, Managing and planning the funds within the organization',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Marketing & Public Relations',
        color: '#997265',
        logo: 'https://s3.ap-southeast-1.amazonaws.com/project-xcidic4.0-bucket/assets/department/marketing.svg',
        description:
          'Monitor, assist, and solve problem that offer value to customer',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Departments', null, {});
  },
};
