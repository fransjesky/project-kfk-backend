'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Journeys', [
      {
        year: '2014',
        text: 'Founded in Singapore and focused on website development until 2016',
        mainColor: '#FF9D42',
        secondaryColor: '#FFB571',
        dimension: 85,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        year: '2016',
        text: 'Established in Jakarta, Indonesia',
        mainColor: '#D6565A',
        secondaryColor: '#DD7377',
        dimension: 96,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        year: '2017',
        text: 'First mobile application development',
        mainColor: '#4D82F3',
        secondaryColor: '#7EA5F8',
        dimension: 110,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        year: '2018',
        text: 'First IoT and system integration development',
        mainColor: '#419E6A',
        secondaryColor: '#A5E1A8',
        dimension: 125,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        year: '2020',
        text: 'Xcidic started partnership with startups',
        mainColor: '#EFB008',
        secondaryColor: '#FFDC81',
        dimension: 145,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        year: '2021',
        text: 'First government project',
        mainColor: '#94A3B8',
        secondaryColor: '#CBD4E1',
        dimension: 160,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Journeys', null, {});
  },
};
