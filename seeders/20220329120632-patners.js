'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Patners', [
      {
        name: 'Kementerian Luar Negeri',
        logo: 'https://s3.ap-southeast-1.amazonaws.com/project-xcidic4.0-bucket/assets/partnerA.png',
        order: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Campaign',
        logo: 'https://s3.ap-southeast-1.amazonaws.com/project-xcidic4.0-bucket/assets/campaign.png',
        order: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'FitCells',
        logo: 'https://s3.ap-southeast-1.amazonaws.com/project-xcidic4.0-bucket/assets/fitcells.png',
        order: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'BTFV',
        logo: 'https://s3.ap-southeast-1.amazonaws.com/project-xcidic4.0-bucket/assets/BTFV.png',
        order: 4,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Toffee Dev',
        logo: 'https://s3.ap-southeast-1.amazonaws.com/project-xcidic4.0-bucket/assets/toffeedev.png',
        order: 5,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'White Source',
        logo: 'https://s3.ap-southeast-1.amazonaws.com/project-xcidic4.0-bucket/assets/white+source.png',
        order: 6,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'UpCloud',
        logo: 'https://s3.ap-southeast-1.amazonaws.com/project-xcidic4.0-bucket/assets/upclound.png',
        order: 7,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Patners', null, {});
  },
};
