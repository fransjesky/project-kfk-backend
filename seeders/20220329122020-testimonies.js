'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Testimonies', [
      {
        name: 'John See Toh',
        position: 'Chairman Runninghour Co-operative',
        companyName: 'Runninghour Co-operative',
        clientTestimony:
          'We would like to thank Xcidic for doing such a wonderful job in revamping our website. We absolutely love the artistic presentation and more importantly in taking the time to understand our organisation in order to help us reach out to our target audience. The consultants are friendly and helpful. We look forward to your continuous support.',
        photo:
          'https://s3.ap-southeast-1.amazonaws.com/project-xcidic4.0-bucket/assets/jonh+see+toh.png',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Sky Lee',
        position: 'Founder of SB Rope Access Specialist',
        companyName: 'SB Rope Access Specialist',
        clientTestimony:
          'Xcidic is a very helpful and responsible web development company. Especially during the UAT phase, all issues logged was promptly and accurately addressed. Whenever there were any requirements which they were not sure of, they would promptly check with us in order to clarify and get things right the first time. They provided many genuine and helpful advices which enabled us to streamline our web development',
        photo:
          'https://s3.ap-southeast-1.amazonaws.com/project-xcidic4.0-bucket/assets/sky+lee.png',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Stanley Soh',
        position: 'Host/MC/Talent Manager',
        companyName: 'Stanley',
        clientTestimony:
          'I would like to thank Xcidic for creating a professional and chic website for me! The feedback from my clients about the website was positive! Despite my busy schedule, they were very accommodating of my requests! I absolutely love the initiatives and ideas of the Xcidic Team! I will definitely recommend their services to everyone!.',
        photo:
          'https://s3.ap-southeast-1.amazonaws.com/project-xcidic4.0-bucket/assets/stanley.png',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Testimonies', null, {});
  },
};
