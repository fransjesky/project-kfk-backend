'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Settings', [
      {
        optionName: 'isEnableEnquiries',
        optionValue: 'true',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        optionName: 'recipientEmail',
        optionValue: 'admin@xcidic.com',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        optionName: 'senderEmail',
        optionValue: 'admin@xcidic.com',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Settings', null, {});
  },
};
