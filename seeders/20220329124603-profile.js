'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Profiles', [
      {
        title: 'Company Profile',
        description:
          'Xcidic is a reliable tech company who transforms your conventional business into digital platforms by customizing your web and mobile applications with excellent UI/UX design, seamless system integration, and secured platform.  Our team consists of a group of tech professionals who are beyond passionate in solving various problems by making the best use of existing technologies in order to curate simple yet effective, secured and integrated solutions from our lab. ',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Profiles', null, {});
  },
};
