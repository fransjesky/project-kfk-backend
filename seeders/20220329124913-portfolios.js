'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Portfolios', [
      {
        name: 'Ootopia',
        description:
          'Oootopia adalah perusahaan asal Hong Kong yang memiliki beberapa properti yang beroperasi sebagai serviced apartment. Oootopia adalah perusahaan asal Hong Kong yang memiliki beberapa properti yang beroperasi sebagai serviced apartment.',
        projectCompany: 'Oootopia',
        projectType: ['Mobile', 'Website'],
        detailLink: 'https://www.behance.net/gallery/138202463/Oootopia',
        thumbnail:
          'https://s3.ap-southeast-1.amazonaws.com/project-xcidic4.0-bucket/assets/works-oootopia.png',
        status: 'active',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'INA-Access',
        description:
          'INA-Access adalah project KBRI (Kedutaan Besar Republic Indonesia)',
        projectCompany: 'Ina-Access',
        projectType: ['Mobile', 'Website', 'IOT'],
        detailLink: 'https://www.behance.net/gallery/138860661/INA-Access',
        thumbnail:
          'https://s3.ap-southeast-1.amazonaws.com/project-xcidic4.0-bucket/assets/works-inacess.png',
        status: 'active',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'KBRI Lima Website',
        description:
          'KBRI Lima website adalah website untuk kementerian luar negri indonesia peru dan bolivia di lima',
        projectCompany: 'KBRI Lima',
        projectType: ['Website'],
        detailLink:
          'https://www.behance.net/gallery/139193397/KBRI-Lima-Project',
        thumbnail:
          'https://s3.ap-southeast-1.amazonaws.com/project-xcidic4.0-bucket/development/attachments/1647933706150-attachment',
        status: 'active',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Portfolios', null, {});
  },
};
